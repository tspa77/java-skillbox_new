package com.tspa77.employees;

public enum EmployeePosition {
    TOP_MANGER,
    SALES_MANGER,
    OPERATOR
}
