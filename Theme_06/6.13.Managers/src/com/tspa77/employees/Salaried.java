package com.tspa77.employees;

public interface Salaried {
    Long getMonthSalary();
}
