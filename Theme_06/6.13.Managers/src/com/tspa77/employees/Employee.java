package com.tspa77.employees;

public abstract class Employee implements Salaried {
    private long employeeNumber;    // табельнеый номер
    private String name;            // имя
    private long fixedSalary;       // оклад
    private long payslipLastMonth;  // начисленная ЗП за последний месяц


    public Employee(long employeeNumber, String name, long fixedSalary) {
        this.employeeNumber = employeeNumber;
        this.name = name;
        this.fixedSalary = fixedSalary;
    }


    @Override
    public Long getMonthSalary() {
        return payslipLastMonth;
    }


    public String getName() {
        return name;
    }

    public long getFixedSalary() {
        return fixedSalary;
    }

    public void setPayslipLastMonth(long payslipLastMonth) {
        this.payslipLastMonth = payslipLastMonth;
    }


    @Override
    public String toString() {
        return "Employee{" +
                ", employeeNumber=" + employeeNumber +
                ", name='" + name + '\'' +
                ", fixedSalary=" + fixedSalary +
                '}';
    }
}
