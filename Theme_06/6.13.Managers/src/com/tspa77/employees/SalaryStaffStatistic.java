package com.tspa77.employees;

public interface SalaryStaffStatistic {

    void getTopSalaryStaff(int count);

    void getLowestSalaryStaff(int count);
}
