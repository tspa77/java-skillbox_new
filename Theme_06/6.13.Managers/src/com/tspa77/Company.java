package com.tspa77;

import com.tspa77.employees.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Company implements SalaryStaffStatistic {
    private static final int QUOTA_TOP_MANAGERS = 10;    // квота сотрудников для должности в процентах
    private static final int QUOTA_SALES_MANAGERS = 40;

    private static final double SALES_MANAGER_AWARD_RATIO = 0.05;  // коэффициент премии для менеджеров по продажам принятый
    // в компании (5% от суммы которую он заработал для компании)

    private static final double TOP_MANAGER_AWARD_RATIO = 0.01; // коэффициент премии для Топ менеджеров принятый
    // в компании (3% от общей прибыли компании)

    private static final int LOWER_THRESHOLD_FOR_TOP_MANAGER_AWARD = 10_000_000; // порог, при достижении которого


    private String name;
    private long incomeLastMonth;       // прибыль компании за прошлый месяц
    private long salariesOfAllEmployeesLastMonth; // расходы на выплату ЗП
    private List<Employee> employees;
    private HR hr;
    private AccountsDepartment accountsDepartment;


    public Company(String name) {
        this.name = name;                       // имя компании
        employees = new ArrayList<>();          // создаём список сотрудников
        hr = new HR();                          // создаём отдел кадров
        accountsDepartment = new AccountsDepartment(); // бухгалтерия
    }


    public void run() {

        // Набираем персонал в новую компанию. 270 человек.
        findEmployees(270);

        // Смотрим
        printListEmployees();

        // Верочка, покажите-ка мне полное штатное расписание...
        hr.printPersonalData();

        // Зинаида Павловна, прошу начислить сотрудникам ЗП за месяц и разослать расчётные листы...
        accountsDepartment.calculateWages();

        // И что там вышло?
        accountsDepartment.printMonthSalaryEmployees();

        // Ну и общий баланс по результатам месяца?
        accountsDepartment.printMonthBalanceCompany();

        // Не так не пойдёт! Проводим оптимизация. Приказываю сократить 50 человек
        layEmployees(50);

        // Смотрим что осталось
        printListEmployees();

        // Топ
        getTopSalaryStaff(10);

        // АнтиТоп
        getLowestSalaryStaff(5);

    }


    /**
     * Печатает список сотрудников
     */
    private void printListEmployees() {
        for (Employee employee : employees) {
            System.out.println(employee.getName());
        }
        System.out.printf("Всего %d сотрудников\n\n", employees.size());
    }

    /**
     * Распоряжение в отдел кадров нанять необходимое количество сотрудников
     */
    private void findEmployees(int quantity) {
        // Пропорции согласно штатному расписанию
        int topManagerQuantity = quantity / 100 * QUOTA_TOP_MANAGERS;
        for (int i = 0; i < topManagerQuantity; i++) {
            hr.hireInTheCompany(EmployeePosition.TOP_MANGER);
        }

        int salesManagerQuantity = quantity / 100 * QUOTA_SALES_MANAGERS;
        for (int i = 0; i < salesManagerQuantity; i++) {
            hr.hireInTheCompany(EmployeePosition.SALES_MANGER);
        }

        int operatorQuantity = quantity - topManagerQuantity - salesManagerQuantity;
        for (int i = 0; i < operatorQuantity; i++) {
            hr.hireInTheCompany(EmployeePosition.OPERATOR);
        }
    }


    private void layEmployees(int quantity) {
        Random random = new Random();
        if (quantity < employees.size()) {
            for (int i = 0; i < quantity; i++) {
                hr.layOffTheCompany(employees.get(random.nextInt(employees.size())));
            }
        }
    }


    @Override
    public void getLowestSalaryStaff(int count) {
        System.out.printf("\n%s самых низкоплачиваемых сотрудников\n", count);
        employees.stream()
                .sorted(Comparator.comparing(Employee::getMonthSalary))
                .limit(count)
                .forEach(e -> System.out.printf("%s  %,d\n", e.getName(), e.getMonthSalary()));
    }


    @Override
    public void getTopSalaryStaff(int count) {
        System.out.printf("\n%s самых высооплачиваемых сотрудников \n", count);
        employees.stream()
                .sorted(Comparator.comparing(Employee::getMonthSalary).reversed())
                .limit(count)
                .forEach(e -> System.out.printf("%s  %,d\n", e.getName(), e.getMonthSalary()));
    }

    /**
     * Отдел кадров
     */
    private class HR {
        // Оклады должностей. Да не финал, ибо время идёт, зарплаты меняются...
        private long minSalTopManger = 200_000;
        private long minSalSalesManger = 50_000;
        private long minSalOperator = 20_000;

        private int maxUpperLimitPercent = 25; // потолок оклада в процентах от минимального, т.е. вилка


        private void hireInTheCompany(EmployeePosition employeePosition) {
            // Генератор фэйкового табельного номера и заодно и имени
            int number = employees.size() + 1;
            Random random = new Random();
            long personalAllowance;

            switch (employeePosition) {
                case TOP_MANGER:
                    personalAllowance = minSalTopManger / 100 * random.nextInt(maxUpperLimitPercent);
                    employees.add(new TopManager(number, "Сотрудник № " + number,
                            minSalTopManger + personalAllowance));
                    break;
                case SALES_MANGER:
                    personalAllowance = minSalSalesManger / 100 * random.nextInt(maxUpperLimitPercent);
                    employees.add(new SalesManager(number, "Сотрудник № " + number,
                            minSalSalesManger + personalAllowance));
                    break;
                case OPERATOR:
                    personalAllowance = minSalOperator / 100 * random.nextInt(maxUpperLimitPercent);
                    employees.add(new SalesManager(number, "Сотрудник № " + number,
                            minSalOperator + personalAllowance));
                    break;
            }
        }


        private void layOffTheCompany(Employee employee) {
            employees.remove(employee);
        }


        private void printPersonalData() {
            for (Employee employee : employees) {
                System.out.println(employee.toString());
            }
            System.out.printf("Всего %d сотрудников\n\n", employees.size());
        }
    }


    /**
     * Бухгалтерия
     */
    private class AccountsDepartment {

        /**
         * Рассчитываем ЗП и рассылаем расчётные листы
         * Да, нужно под каждое действие отделные методы, но уж слишком сложно будет с текущей архитектурой
         */
        void calculateWages() {
            for (Employee employee : employees) {
                // Расчитываем для операторов
                if (employee instanceof Operator) {
                    employee.setPayslipLastMonth(employee.getFixedSalary()); // тут при необходимости можем применить
                    // районый коэффициент и удержать подоходный и профсоюзные
                }

                if (employee instanceof SalesManager) {
                    employee.setPayslipLastMonth((long) (employee.getFixedSalary() + getProfitEmployee(employee)
                            * SALES_MANAGER_AWARD_RATIO));
                    // приведение к лонг отбрасывает знаки после запятой - так и задумано, округление в пользу компании
                }

                salariesOfAllEmployeesLastMonth += employee.getMonthSalary(); // сразу добавляем
            }

            // Если продажники заработали на премию топам, то начисляем её. Но оклад в любом случае.
            for (Employee employee : employees) {
                if (employee instanceof TopManager) {
                    long salary = employee.getFixedSalary();
                    if (incomeLastMonth > LOWER_THRESHOLD_FOR_TOP_MANAGER_AWARD) {
                        salary += incomeLastMonth * TOP_MANAGER_AWARD_RATIO;
                    }
                    employee.setPayslipLastMonth(salary);
                    salariesOfAllEmployeesLastMonth += salary;
                }
            }
        }


        /**
         * Возвращает сумму которую сотрудник заработал для компании в прошллом месяце
         * Метод-заглушка ¯\_(ツ)_/¯, поэтому и принимаемый employee тут игнорируется
         */
        private long getProfitEmployee(Employee employee) {
            Random random = new Random();
            int profit = random.nextInt(500_000); // Да я использую магическую константу. Но это фейкогенератор.
            // Не хочу из-за него вносить константы в реальные классы

            // Так же задним числом добавляем эту сумму в прибыль компании за прошлый месяц
            incomeLastMonth += profit;
            // Когда пробежится весь цикл по продажникам - получим полученную прибыль. Простите за тафтологию :)

            return profit;
        }


        private void printMonthSalaryEmployees() {
            for (Employee employee : employees) {
                System.out.println(employee.getName() + "; " + employee.getMonthSalary());
            }
        }


        private void printMonthBalanceCompany() {
            System.out.printf("\nПрибыль компании за месяц:\t\t\t %,20d" +
                            "\nЗатраты на выплату заработной платы: %,20d" +
                            "\nЧистая прибыль за месяц:\t\t\t %,20d\n\n", incomeLastMonth,
                    salariesOfAllEmployeesLastMonth, (incomeLastMonth - salariesOfAllEmployeesLastMonth));
        }
    }
}
