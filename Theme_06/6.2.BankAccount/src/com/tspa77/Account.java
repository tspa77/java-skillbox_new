package com.tspa77;

public class Account {
    private long amount; //в копейках


    public boolean withdrawFromAccount(long sum) {
        if (withdrawIsAllowed(sum)) {
            amount -= sum;
            return true;
        }
        return false;
    }


    protected boolean withdrawIsAllowed(long sum) {
        if (sum <= amount) {
            return true;
        } else {
            System.out.println("Недостаточно средств на счёте. Обратитесь в " +
                    "ближайшее отдлеление для подключения овердрафта");
            return false;
        }

        // todo Проверка транзакции службой безопасности
    }


    public boolean putOnAccount(long sum) {
        if (putIsAllowed(sum)) {
            amount += sum;
            return true;
        }
        return false;
    }


    protected boolean putIsAllowed(long sum) {
        return sum > 0;
    }


    public void printAmount() {
        System.out.printf("Баланс: %s рублей\n", amount / 100);
    }
}


//        System.out.println("Пополнение прошло успешно");
//            System.out.println("Списание прошло успешно");