package com.tspa77;

public class CardAccount extends Account {
    // Комиссия банка (в процентах) при снятии денег
    private final static double COMMISSION_IN_PERCENT = 1.;


    @Override
    public boolean withdrawFromAccount(long sum) {
        long commissionOfBank = (long) Math.ceil(sum / 100. * COMMISSION_IN_PERCENT);
        return super.withdrawFromAccount(sum + commissionOfBank);
    }
}
