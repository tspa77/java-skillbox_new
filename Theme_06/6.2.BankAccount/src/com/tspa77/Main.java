package com.tspa77;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Account> accounts = new ArrayList<>();

        accounts.add(new Account());
        accounts.add(new CardAccount());
        accounts.add(new DepositoryAccount());

        // плюс/минус несколько дней от даты +1 месяц
        for (int i = 29; i < 32; i++) {
            DepositoryAccount depositoryAccount = new DepositoryAccount(LocalDate.now().minusDays(i));
            accounts.add(depositoryAccount);
        }

        // тестирование
        int testSum1 = 100_00;
        int testSum2 = 20_00;
        for (Account account : accounts) {
            account.printAmount();

            System.out.printf("Пробуем зачислить %d рублей\n", testSum1 / 100);
            System.out.println(account.putOnAccount(testSum1));
            account.printAmount();

            System.out.printf("Пробуем снять %d рублей\n", testSum1 / 100);
            System.out.println(account.withdrawFromAccount(testSum1));
            account.printAmount();

            System.out.printf("Пробуем снять %d рублей\n", testSum2 / 100);
            System.out.println(account.withdrawFromAccount(testSum2));
            account.printAmount();

            System.out.println("-----------");
        }
    }
}
