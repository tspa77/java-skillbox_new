package com.tspa77;

import java.time.LocalDate;
import java.time.Period;

public class DepositoryAccount extends Account {
    private LocalDate openingDate;


    DepositoryAccount() {
        openingDate = LocalDate.now();
    }

    // todo Отключить конструтор перед продакшеном. Он для тестов
    DepositoryAccount(LocalDate openingDate) {
        this.openingDate = openingDate;
    }


    @Override
    protected boolean withdrawIsAllowed(long sum) {
        // дополнительная проверка для депозитного счёта
        if (!overAMonth()) {
            return false;
        }
        return super.withdrawIsAllowed(sum);
    }

    private boolean overAMonth() {
        // todo убрать debug print
        System.out.printf("Счёт открыт %s. ", openingDate);

        long differenceInMonths = Period.between(openingDate, LocalDate.now()).toTotalMonths();

        if (differenceInMonths < 1) {
            System.out.println("Снятие денег с депозитарного счёта " +
                    "возможно только после 1 месяца");
            return false;
        }
        return true;
    }
}
