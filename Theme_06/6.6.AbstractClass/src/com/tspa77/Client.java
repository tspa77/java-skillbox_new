package com.tspa77;

import java.util.Random;

abstract class Client {
    private long accountNumber;
    private double accountBalance;


    Client() {
        accountNumber = new Random().nextLong();
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void getAccountBalance() {
        System.out.printf("Баланс: %.2f\n", accountBalance);
    }

    void withdrawFromAccount(double sum) {
        if (sum <= accountBalance) {
            accountBalance -= sum;
            System.out.println("Списание прошло успешно");

        } else {
            System.out.println("Недостаточно средств на счёте. Обратитесь в " +
                    "ближайшее отдлеление для подключения овердрафта");
        }
    }

    void putOnAccount(double sum) {
        accountBalance += sum;
        System.out.println("Пополнение прошло успешно");
    }

}

// Абстрактным методам применения не нашлось.
