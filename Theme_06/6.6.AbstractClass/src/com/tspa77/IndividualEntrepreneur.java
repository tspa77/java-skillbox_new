package com.tspa77;

public class IndividualEntrepreneur extends Client {
    @Override
    void putOnAccount(double sum) {
        double multiplier = sum < 1000 ? 0.01 : 0.005;
        super.putOnAccount(sum - sum * multiplier);
    }
}
