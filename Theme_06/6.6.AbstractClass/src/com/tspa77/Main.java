package com.tspa77;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Client> clients = new ArrayList<>();

        clients.add(new NaturalPerson());
        clients.add(new LegalEntities());
        clients.add(new IndividualEntrepreneur());

        for (Client client : clients) {
            System.out.println(client.getAccountNumber());
            client.getAccountBalance();

            System.out.println("+100");
            client.putOnAccount(100);
            client.getAccountBalance();

            System.out.println("+1000");
            client.putOnAccount(1000);
            client.getAccountBalance();

            System.out.println("+1500");
            client.putOnAccount(1500);
            client.getAccountBalance();

            System.out.println("-100");
            client.withdrawFromAccount(100);
            client.getAccountBalance();

            System.out.println("-1000");
            client.withdrawFromAccount(1000);
            client.getAccountBalance();
            System.out.println("-----------");
        }
    }
}
