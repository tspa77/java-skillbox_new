package com.tspa77.kotlin

import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Table

@Embeddable
@Table(name = "subscriptions")
data class Subscriptions(

    @Column(name = "student_id")
    var studentId: Int,

    @Column(name = "course_id")
    var courseId: Int,

    @Column(name = "subscription_date")
    var subscriptionDate: Date
)
