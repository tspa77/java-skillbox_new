package com.tspa77.kotlin

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "students")
data class Student(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int,

    @Column(name = "name")
    var name: String,

    @Column(name = "age")
    var age: Int,

    @Column(name = "registration_date")
    var registrationDate: Date
) {

    override fun equals(other: Any?): Boolean {
        // 1
        if (this === other) {
            return true
        }

        // 2
        if (other == null || javaClass != other::class.java) {
            return false
        }

        // 3
        val student = other as Student
        return name == student.name && age == student.age &&
                registrationDate == student.registrationDate
    }

    override fun hashCode(): Int {
        return ((42 + name.hashCode()) * 42 + age) * 42 + registrationDate.hashCode()
    }
}
