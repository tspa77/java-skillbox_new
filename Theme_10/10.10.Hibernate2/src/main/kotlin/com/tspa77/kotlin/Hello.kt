package com.tspa77.kotlin

import org.hibernate.cfg.Configuration

fun main() {

    val config = Configuration()
    config.addAnnotatedClass(Course::class.java)
    config.addAnnotatedClass(CourseType::class.java)
    config.addAnnotatedClass(Teacher::class.java)
    config.addAnnotatedClass(Student::class.java)
    config.addAnnotatedClass(Subscriptions::class.java)
    config.configure("hibernate.cfg.xml")

    val sessionFactory = config.buildSessionFactory()
    val session = sessionFactory.openSession()

    sessionFactory.use {
        session.use { itS ->
            val criteriaBuilder = itS.criteriaBuilder
            // Courses
            val criteriaQuery = criteriaBuilder.createQuery(Course::class.java)
            val root = criteriaQuery.from(Course::class.java)
            criteriaQuery.select(root)
            val query = itS.createQuery(criteriaQuery)
            val listCourses = query.resultList


            val currentCourse = listCourses[4]
            println(currentCourse.description)
            println("Преподаватель: ${currentCourse.teacher.name}")
            println("Студентов на курсе: ${currentCourse.students.size}")
            currentCourse.students.forEach { println("\t\t${it.name}") }
        }
    }
}
