package com.tspa77.kotlin

import javax.persistence.*

@Entity
@Table(name = "courses")
data class Course(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int,

    @Column(name = "name")
    var name: String,

    @Column(name = "duration")
    var duration: Int,

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum", name = "type")
    var type: CourseType,

    @Column(name = "description")
    var description: String,

    @ManyToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var teacher: Teacher,

    @Column(name = "students_count")
    var studentsCount: Int,

    @Column(name = "price")
    var price: Int,

    @Column(name = "price_per_hour")
    var pricePerHour: Float,

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "subscriptions",
        joinColumns = [JoinColumn(name = "course_id")],
        inverseJoinColumns = [JoinColumn(name = "student_id")]
    )
    var students: Set<Student>
)
