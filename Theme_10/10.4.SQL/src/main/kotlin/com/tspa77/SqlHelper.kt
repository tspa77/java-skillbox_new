package com.tspa77

import java.sql.DriverManager

object SqlHelper {
    private const val URL = "jdbc:mysql://localhost:3306/skillbox" +
            "?verifyServerCertificate=false" +
            "&useSSL=false" +
            "&requireSSL=false" +
            "&allowPublicKeyRetrieval=true" +
            "&useLegacyDatetimeCode=false" +
            "&amp" +
            "&serverTimezone=UTC"
    private const val USER = "root"
    private const val PASSWORD = "123Qwerty"
    private val REQUEST = """
        SELECT courses.name, 
            COUNT(*) / (SELECT EXTRACT(MONTH FROM MAX(subscription_date)) FROM subscriptions ) as count 
        FROM courses 
        JOIN purchaselist ON purchaselist.course_name = courses.name 
        GROUP BY courses.name
    """.trimIndent()

    fun print() {
        val connection = DriverManager.getConnection(URL, USER, PASSWORD)
        connection.use {
            val statement = connection.createStatement()
            statement.use {
                val resultSet = statement.executeQuery(REQUEST)
                resultSet.use {
                    while (resultSet.next()) {
                        println(resultSet.getString("name") + " " + resultSet.getString("count"))
                    }
                }
            }
        }
    }
}
