package com.tspa77.java;

import com.tspa77.java.Course;
import com.tspa77.java.CourseType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class Jmain {
    public static void main(String[] args) {

//        Configuration config = new Configuration();
//        config.configure("hibernate.cfg.xml");
//        // Добавляем Entity классы
//        config.addAnnotatedClass(Course.class);
//        config.addAnnotatedClass(CourseType.class);
//
//        // Регистрируем сервис
//        ServiceRegistry registry = new StandardServiceRegistryBuilder()
//                .applySettings(config.getProperties())
//                .build();

        ServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();

        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

        Session session = sessionFactory.openSession();

        Course course = session.get(Course.class,1);
        System.out.println(course.getDescription());

        session.close();
        sessionFactory.close();
    }
}
