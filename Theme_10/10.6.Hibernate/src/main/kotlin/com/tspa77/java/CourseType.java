package com.tspa77.java;

public enum CourseType {
    PROGRAMMING,
    DESIGN,
    MARKETING,
    MANAGEMENT,
    BUSINESS,
}
