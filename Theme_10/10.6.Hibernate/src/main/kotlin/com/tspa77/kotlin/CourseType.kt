package com.tspa77.kotlin

enum class CourseType {
    PROGRAMMING,
    DESIGN,
    MARKETING,
    MANAGEMENT,
    BUSINESS,
}
