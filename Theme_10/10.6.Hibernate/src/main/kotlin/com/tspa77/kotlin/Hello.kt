package com.tspa77.kotlin

import org.hibernate.boot.MetadataSources
import org.hibernate.boot.registry.StandardServiceRegistryBuilder

fun main() {
    // Создаем инстанс конфигурации и загружаем конфиг из файла
//    val config = Configuration()
//    config.configure("hibernate.cfg.xml")
//    // Добавляем Entity классы
//    config.addAnnotatedClass(Course::class.java)
//    config.addAnnotatedClass(CourseType::class.java)
    // НЕ РАБОТАЕТ!
    // Exception in thread "main" org.hibernate.UnknownEntityTypeException: Unable to locate persister: com.tspa77.java.Course

    val registry = StandardServiceRegistryBuilder()
        .configure("hibernate.cfg.xml")
//        .applySettings(config.properties)
        .build()

    val metadata = MetadataSources(registry).metadataBuilder.build()
    val sessionFactory = metadata.sessionFactoryBuilder.build()
    val session = sessionFactory.openSession()

    sessionFactory.use {
        session.use {itS->
            val criteriaBuilder = itS.criteriaBuilder
            // Courses
            val criteriaQuery = criteriaBuilder.createQuery(Course::class.java)
            val root = criteriaQuery.from(Course::class.java)
            criteriaQuery.select(root)
            val query = itS.createQuery(criteriaQuery)
            val list = query.resultList
            list.forEach { println(it) }
        }
    }
}


