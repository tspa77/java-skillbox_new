package com.tspa77.kotlin

import javax.persistence.*

@Entity
@Table(name = "teachers")
data class Teacher(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int,

    @Column(name = "name")
    var name: String,

    @Column(name = "salary")
    var salary: Int,

    @Column(name = "age")
    var age: Int
)
