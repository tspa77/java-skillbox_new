package com.tspa77;

public class Main {

    public static void main(String[] args) {

        int a = 10;
        int b = 11;
        int c = 12;

        int min;
        int mid;
        int max;

        if (a <= b) {
            if (b <= c) {
                min = a;
                mid = b;
                max = c;
            } else {
                max = b;
                if (a <= c) {
                    min = a;
                    mid = c;
                } else {
                    min = c;
                    mid = a;
                }
            }
        } else {
            if (a <= c) {
                min = a;
                mid = b;
                max = c;
            } else {
                max = a;
                if (b <= c) {
                    min = b;
                    mid = c;
                } else {
                    min = c;
                    mid = b;
                }
            }
        }

        System.out.println("min: " + min);
        System.out.println("mid: " + mid);
        System.out.println("max: " + max);
    }
}
