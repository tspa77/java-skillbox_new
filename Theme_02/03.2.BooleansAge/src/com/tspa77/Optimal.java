package com.tspa77;

public class Optimal {

    public static void main(String[] args) {

        int a = 3;
        int b = 7;
        int c = 5;

        int min;
        int mid;
        int max;

        if (b >= c) {
            max = b;
            mid = c;
        } else {
            max = c;
            mid = b;
        }
        if (a <= mid) {
            min = a;
        } else {
            min = mid;
            if (a <= max) {
                mid = a;
            } else {
                mid = max;
                max = a;
            }
        }

        System.out.println("min: " + min);
        System.out.println("mid: " + mid);
        System.out.println("max: " + max);
    }


}
