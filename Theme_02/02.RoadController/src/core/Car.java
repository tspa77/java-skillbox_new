package core;

public class Car
{
    // создаётся переменная типа String
    public String number;
    // создаётся переменная типа int
    public int height;
    // создаётся переменная типа double
    public double weight;
    // создаётся переменная типа boolean
    public boolean hasVehicle;
    // создаётся переменная типа boolean
    public boolean isSpecial;

    public String toString()
    {
        String special = isSpecial ? "СПЕЦТРАНСПОРТ " : "";
        return "\n=========================================\n" +
            special + "Автомобиль с номером " + number +
            ":\n\tВысота: " + height + " мм\n\tМасса: " + weight + " кг";
    }
}