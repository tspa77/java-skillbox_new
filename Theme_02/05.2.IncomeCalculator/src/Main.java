import java.util.Scanner;

public class Main {
    // Задаются константы
    private static int minIncome = 200000;  // минимальный доход
    private static int maxIncome = 900000;  // максимальный доход

    private static int officeRentCharge = 140000;   // плата за аренду офиса
    private static int telephonyCharge = 12000;     // плата за телефон
    private static int internetAccessCharge = 7200; // плата за интернет

    private static int assistantSalary = 45000;     // зп помощника
    private static int financeManagerSalary = 90000;// зп фин. управляющего

    private static double mainTaxPercent = 0.24;    // основной налоговый процент
    private static double managerPercent = 0.15;    // доля менеджера

    private static double minInvestmentsAmount = 100000; // минимальная сумма инвестиций

    public static void main(String[] args) {
        // Расчёт минимального порогоа для возможности инвестирования
        double minIncomeForCanMakeInvestments = (calculateFixedCharges()
                + minInvestmentsAmount - calculateFixedCharges() * mainTaxPercent)
                / ((-1 + managerPercent) * (-1 + mainTaxPercent));

        // Форматированный вывод на экран
        System.out.printf("Совет дня: Для начала инвестирования необходима сумма в %,.2f рублей\n\n", minIncomeForCanMakeInvestments);


        while (true) // бесконечный цикл
        {
            System.out.printf("Введите сумму доходов компании за месяц " +     // вывод сообщения на экран
                    //    "(от 200 до 900 тысяч рублей): ");                   // захардкожены значения в тексте
                    "(от %,d до %,d рублей): ", minIncome, maxIncome);         // исправлены с форматированием
            int income = (new Scanner(System.in)).nextInt();                   // получаем введённое с клавиатуры значение и преобразовываем к типу int

            if (!checkIncomeRange(income)) {                                    // отправляем полученное значение на проверку в метод checkIncomeRange
                continue;                                                       // если возвращяемое значение не "true" переходим к новой итерации цикла
            }                                                                   // т.е. фактически начинаем программу заново

            double managerSalary = income * managerPercent;                     // расчитываем зп менеджера по прадажам (он на сделке, получает долю от суммы договора)
            double pureIncome = income - managerSalary -                        // получаем чистый доход
                    calculateFixedCharges();
            double taxAmount = mainTaxPercent * pureIncome;                     // расчитываем налог на него
            double pureIncomeAfterTax = pureIncome - taxAmount;                 // получаем чистый доход, после вычета налога

            boolean canMakeInvestments = pureIncomeAfterTax >=                  // если сумма чистого дохода, после вычета налога больше чем минимальная сумма инвестиций
                    minInvestmentsAmount;                                       // то в логическую переменную canMakeInvestments (возможность инвестировать) присваивается "true"

            System.out.println("Зарплата менеджера: " + managerSalary);         // вывод на экран
            System.out.println("Общая сумма налогов: " +                        // Т.к. чистый доход может уйти в минус, то и налог расчитается как отрицательный
                    (taxAmount > 0 ? taxAmount : 0));                           // тернарный оператор выдаёт 0, если налог отрицательный
            System.out.println("Компания может инвестировать: " +
                    (canMakeInvestments ? "да" : "нет"));                       // выбор да/нет через тернарный оператор для соответсвующих значений false/true
            if (pureIncome < 0) {
                System.out.println("Бюджет в минусе! Нужно срочно зарабатывать!");
            }
        }
    }

    private static boolean checkIncomeRange(int income) {       // проверка входного значения
        if (income < minIncome) {                               // если меньше или больше пороговых значений, то метод возвращает false
            System.out.println("Доход меньше нижней границы");  // если входит в диапазон, то true
            return false;
        }
        if (income > maxIncome) {
            System.out.println("Доход выше верхней границы");
            return false;
        }
        return true;
    }

    private static int calculateFixedCharges() {            // суммируются все фиксированные (не процентные) траты
        return officeRentCharge +
                telephonyCharge +
                internetAccessCharge +
                assistantSalary +
                financeManagerSalary;
    }
}
