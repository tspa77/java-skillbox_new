package com.tspa77;

public class Main {

    public static void main(String[] args) {

        int a = 200_000;
        int b = 210_000;

        cycleFor(a, b);
        cycleWhile(a, b);

        a = 220_000;
        b = 235_000;

        cycleFor(a, b);
        cycleWhile(a, b);
    }


    private static void cycleFor(int a, int b) {
        System.out.println("\n***** Start of cycle 'for' *****\n");
        for (int i = a; i <= b; i++) {
            System.out.println("Билет № " + i);
        }
        System.out.println("\n***** End of cycle 'for' *****\n");
    }

    private static void cycleWhile(int a, int b) {
        System.out.println("\n***** End of cycle 'while' *****\n");
        while (a <= b) {
            System.out.println("Билет № " + a);
            a++;
        }
        System.out.println("\n***** End of cycle 'while' *****\n");
    }

}
