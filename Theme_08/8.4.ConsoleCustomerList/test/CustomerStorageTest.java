import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerStorageTest {


    @Test
    public void validateNumberPhone_test() {
        CustomerStorage customerStorage = new CustomerStorage();

        assertFalse(customerStorage.validateNumberPhone("Test"));
        assertFalse(customerStorage.validateNumberPhone("798465"));
        assertTrue(customerStorage.validateNumberPhone("+79215637722"));
        assertTrue(customerStorage.validateNumberPhone("+7 921 563 77 22"));

    }

    @Test
    public void validateEmail_test() {
        CustomerStorage customerStorage = new CustomerStorage();

        assertFalse(customerStorage.validateEmail("@petrov@gmail.com"));
        assertFalse(customerStorage.validateEmail("petrov@gmail"));
        assertFalse(customerStorage.validateEmail(""));
        assertTrue(customerStorage.validateEmail("vasily.petrov@gmail.com"));
        assertTrue(customerStorage.validateEmail("petrov@gmail.ru"));

    }

    @Test
    public void addCustomer_test() {
        CustomerStorage customerStorage = new CustomerStorage();
//      А тут не понял как тестить :( Говорит метод эксэпшенами стреляет....
//      Надеюсь в следующих видео расскажут....

//      customerStorage.addCustomer("add Василий Петров vasily.petrov@gmail.com +79215637722");

    }
}