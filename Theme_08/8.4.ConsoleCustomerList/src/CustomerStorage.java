import exceptions.EmailException;
import exceptions.NumberPhoneException;

import java.util.HashMap;

public class CustomerStorage {
    private HashMap<String, Customer> storage;
    public static final String REGEX_PHONE_NUMBER = "^\\+(?:[0-9] ?){6,14}[0-9]$";
    public static final String REGEX_EMAIL = "^[_A-Za-z]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public CustomerStorage() {
        storage = new HashMap<>();
    }

    public void addCustomer(String data) throws EmailException, NumberPhoneException {
        String[] components = data.split("\\s+");
        if (!validateEmail(components[2]))
            throw new EmailException(components[2]);
        if (!validateNumberPhone(components[3]))
            throw new NumberPhoneException(components[3]);
        String name = components[0] + " " + components[1];
        storage.put(name, new Customer(name, components[3], components[2]));
    }

    public void listCustomers() {
        storage.values().forEach(System.out::println);
    }

    public void removeCustomer(String name) {
        storage.remove(name);
    }

    public int getCount() {
        return storage.size();
    }

    public boolean validateEmail(String email) {
        return email.matches(REGEX_EMAIL);
    }

    public boolean validateNumberPhone(String numberPhone) {
        return numberPhone.matches(REGEX_PHONE_NUMBER);
    }

}