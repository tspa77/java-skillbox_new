package exceptions;

public class NumberPhoneException extends RuntimeException {
    public NumberPhoneException(String numberPhone) {
        super(numberPhone + " - is wrong format phone number!");
    }
}