package exceptions;

public class EmailException extends RuntimeException {
    public EmailException(String email) {
        super(email + " - is wrong format email!");
    }
}