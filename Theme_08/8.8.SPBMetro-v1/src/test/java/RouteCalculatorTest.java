import core.Line;
import core.Station;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class RouteCalculatorTest extends TestCase {

    /**
     * Именование тестов:
     * TestRoute_{Количество станций}{Переход}{Количество станций}{Переход}...
     * Например:
     * TestRoute__5С3С6 - 5 станций, переход, 3 станции, переход, 6 станций
     * <p>
     * Начальная станция также считается!
     * <p>
     * Расчёт:
     * Формула бертёся из имени теста, всё суммируется, но количество
     * станций для каждой линии = n-1
     * т.е.     TestRoute__5С3С6
     * будт расчитываться как
     * 4*s + c + 2*s + c + 5*s
     */

    //========== for CalculateDurationRoute =================

    private Line line1 = new Line(1, "1-я линия");
    private Line line2 = new Line(2, "2-я линия");
    private Line line3 = new Line(3, "3-я линия");

    private Double s = RouteCalculator.interStationDuration;
    private Double c = RouteCalculator.interConnectionDuration;


    //========== for getShortestRoute =================
    //
    //      (Red line - 1)
    //          r1
    //          |
    //   b1 - r2/b2 - o1/b3 - b4   (Blue line - 2)
    //          |        \
    //          r3         o2
    //          |             \
    //   g1 - r4/g2 - g3 - g4 - o3/g5   (Green line - 3)
    //                            \
    //                              o4  (Orange line - 4)
    //

    private RouteCalculator routeCalculator;
    private StationIndex stationIndex;

    // создаём линии
    private Line redLine = new Line(1, "Red line");
    private Line blueLine = new Line(2, "Blue line");
    private Line greenLine = new Line(3, "Green line");
    private Line orangeLine = new Line(4, "Orange line");
    private List<Line> lineList = Arrays.asList(redLine, blueLine, greenLine, orangeLine);


    private Station getStation(String name) {
        return stationIndex.getStation(name);
    }


    private List<Station> makeRoute(String... array) {
        List<Station> route = new ArrayList<>();
        for (String s : array) {
            route.add(getStation(s));
        }
        return route;
    }


    @Before
    public void setUp() throws Exception {
        stationIndex = new StationIndex();

        // создаём станции и добавляем станции в линии
        for (int i = 1; i <= 4; i++) {
            redLine.addStation(new Station("r" + i, redLine));
        }

        for (int i = 1; i <= 4; i++) {
            blueLine.addStation(new Station("b" + i, blueLine));
        }

        for (int i = 1; i <= 5; i++) {
            greenLine.addStation(new Station("g" + i, greenLine));
        }

        for (int i = 1; i <= 4; i++) {
            orangeLine.addStation(new Station("o" + i, orangeLine));
        }

        // добавляем линию в индекс
        for (Line line : lineList) {
            stationIndex.addLine(line);
            // добавляем станции в индекс
            int lineSize = line.getStations().size();
            for (int i = 0; i < lineSize; i++) {
                stationIndex.addStation(line.getStations().get(i));
            }
        }

        // создаём пересадки
        List<Station> connectionStations_r2b2 = new ArrayList<>();
        connectionStations_r2b2.add(getStation("r2"));
        connectionStations_r2b2.add(getStation("b2"));
        stationIndex.addConnection(connectionStations_r2b2);

        List<Station> connectionStations_o1b3 = new ArrayList<>();
        connectionStations_o1b3.add(getStation("o1"));
        connectionStations_o1b3.add(getStation("b3"));
        stationIndex.addConnection(connectionStations_o1b3);

        List<Station> connectionStations_r4g2 = new ArrayList<>();
        connectionStations_r4g2.add(getStation("r4"));
        connectionStations_r4g2.add(getStation("g2"));
        stationIndex.addConnection(connectionStations_r4g2);

        List<Station> connectionStations_o3g5 = new ArrayList<>();
        connectionStations_o3g5.add(getStation("o3"));
        connectionStations_o3g5.add(getStation("g5"));
        stationIndex.addConnection(connectionStations_o3g5);

        // создаём экземпляр калькулятора
        routeCalculator = new RouteCalculator(stationIndex);
    }


    //=========================================================================
    //          testCalculateDurationRoute

    @Test
    // 1) 2 станции - просто проехать одну остановку
    public void testCalculateDurationRoute_2() {
        List<Station> route_2 = new ArrayList<>();
        route_2.add(new Station("1.1", line1));
        route_2.add(new Station("1.2", line1));

        double expected = 1 * s;
        double actual = RouteCalculator.calculateDuration(route_2);
        assertEquals(expected, actual);
    }

    @Test
    // 2) 11 станций
    public void testCalculateDurationRoute_11() {
        List<Station> route_11 = new ArrayList<>();
        for (int i = 1; i <= 11; i++) {
            route_11.add(new Station("1." + i, line1));
        }
        double expected = 10 * s;
        double actual = RouteCalculator.calculateDuration(route_11);
        assertEquals(expected, actual);
    }

    @Test
    // 3) Просто пеший переход между ветками
    public void testCalculateDurationRoute_с() {
        List<Station> route_c = new ArrayList<>();
        route_c.add(new Station("1.1", line1));
        route_c.add(new Station("2.1", line2));

        double expected = c;
        double actual = RouteCalculator.calculateDuration(route_c);
        assertEquals(expected, actual);
    }

    @Test
    // 4) Переход и одна остановка
    public void testCalculateDurationRoute_с2() {
        List<Station> route_c2 = new ArrayList<>();
        route_c2.add(new Station("1.1", line1));
        route_c2.add(new Station("2.1", line2));
        route_c2.add(new Station("2.2", line2));

        double expected = c + 1 * s;
        double actual = RouteCalculator.calculateDuration(route_c2);
        assertEquals(expected, actual);
    }

    @Test
    // 5) 2 станции (одна остановка) и переход
    public void testCalculateDurationRoute_2c() {
        List<Station> route_2c = new ArrayList<>();
        route_2c.add(new Station("1.1", line1));
        route_2c.add(new Station("1.2", line1));
        route_2c.add(new Station("2.1", line2));

        double expected = 1 * s + c;
        double actual = RouteCalculator.calculateDuration(route_2c);
        assertEquals(expected, actual);
    }

    @Test
    // 6) 7-переход-8 --> проспект Ветеранов - Купчино
    public void testCalculateDurationRoute_7c8() {
        List<Station> route_7c8 = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            route_7c8.add(new Station("1." + i, line1));
        }
        for (int i = 1; i <= 8; i++) {
            route_7c8.add(new Station("2." + i, line2));
        }

        double expected = 6 * s + c + 7 * s;
        double actual = RouteCalculator.calculateDuration(route_7c8);
        assertEquals(expected, actual);
    }

    @Test
    // 7) 6-переход-2-переход-5  -->
    // Международная - Садовая || Сенная площадь - Невский проспект || Гостиный двор - Беговая
    public void testCalculateDurationRoute_6c2c5() {
        List<Station> route_6c2c5 = new ArrayList<>();
        for (int i = 1; i <= 6; i++) {
            route_6c2c5.add(new Station("1." + i, line1));
        }
        for (int i = 1; i <= 2; i++) {
            route_6c2c5.add(new Station("2." + i, line2));
        }
        for (int i = 1; i <= 5; i++) {
            route_6c2c5.add(new Station("3." + i, line3));
        }

        double expected = 5 * s + c + 1 * s + c + 4 * s;
        double actual = RouteCalculator.calculateDuration(route_6c2c5);
        assertEquals(expected, actual);
    }


    //=========================================================================
    //        testGetShortestRoute

    @Test
    public void testGetShortestRoute_0connections() {
        List<Station> route = makeRoute("r1", "r2", "r3", "r4");
        assertEquals(route, routeCalculator.getShortestRoute(
                getStation("r1"), getStation("r4")));
    }

    @Test
    public void testGetShortestRoute_1connections() {
        List<Station> route = makeRoute("r1", "r2", "b2", "b3");
        assertEquals(route, routeCalculator.getShortestRoute(
                getStation("r1"), getStation("b3")));
    }

    @Test
    public void testGetShortestRoute_2connections_1() {
        List<Station> route = makeRoute("r1", "r2", "b2", "b3", "o1", "o2", "o3", "o4");
        assertEquals(route, routeCalculator.getShortestRoute(
                getStation("r1"), getStation("o4")));
    }

    public void testGetShortestRoute_2connections_2() {
        List<Station> route2 = makeRoute("r1", "r2", "r3", "r4", "g2", "g3", "g4", "g5");
        assertEquals(route2, routeCalculator.getShortestRoute(
                getStation("r1"), getStation("g5")));
    }

    public void testGetShortestRoute_2connections_3() {
        List<Station> route3 = makeRoute("b4", "b3", "b2", "r2", "r3", "r4", "g2", "g1");
        assertEquals(route3, routeCalculator.getShortestRoute(
                getStation("b4"), getStation("g1")));
    }

}