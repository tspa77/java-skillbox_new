import DirectoryScanner.DirectoryScanner;
import DirectoryScanner.Exporters.ConsolePrinter;
import DirectoryScanner.ListFormatters.CommandLineListFormatter;

/**
 * Написать программу, которая будет измерять размер всего содержимого папки,
 * путь которой передаётся на вход, и выводить его в удобочитаемом виде —
 * в байтах, килобайтах, мегабайтах или гигабайтах.
 */

public class Loader {

    public static void main(String[] args) {

        DirectoryScanner directoryScanner =
                new DirectoryScanner(new ConsolePrinter(), new CommandLineListFormatter());
        directoryScanner.run();
    }
}
