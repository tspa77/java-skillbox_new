package DirectoryScanner.Exporters;

import DirectoryScanner.ListFormatters.ListFormatter;

import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;

public class Exporter extends SimpleFileVisitor<Path> {
    protected ListFormatter listFormatter;

    public ListFormatter getListFormatter() {
        return listFormatter;
    }

    public void setListFormatter(ListFormatter listFormatter) {
        this.listFormatter = listFormatter;
    }
}
