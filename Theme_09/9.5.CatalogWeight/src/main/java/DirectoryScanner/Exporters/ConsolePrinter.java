package DirectoryScanner.Exporters;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;


public class ConsolePrinter extends Exporter {


    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        // TODO use formatter
        System.out.println("!!!!  НЕТ ДОСТУПА   !!!!!!!!!!!! :" + file.getFileName());
        return FileVisitResult.SKIP_SUBTREE;
    }

    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes fileAttributes) {
        System.out.println(listFormatter.format(path));
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes fileAttributes) {
        System.out.println(listFormatter.format(path));
        return FileVisitResult.CONTINUE;
    }


}

