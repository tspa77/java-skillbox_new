package DirectoryScanner.Util;

import java.nio.file.*;
import java.util.Scanner;

public class UserPath {


    public static Path request() {
        Scanner scanner = new Scanner(System.in);
        Path path;

        while (true) {
            System.out.println("Введите путь:");
            String string = scanner.nextLine().trim();
            try {
                path = Paths.get(string);
                if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
                    break;
                }
                System.out.println(path + " - не является путём");
            } catch (InvalidPathException e) {
                System.out.println(string + " - некорректный путь");
            }
        }
        return path;
    }

}


