package DirectoryScanner.ListFormatters;

import java.nio.file.Path;

public class CommandLineListFormatter extends ListFormatter {

    private static final String FOLDER = "<folder>";
    private static final String UNITS = " bytes";
    private static final String PRINT_FILE_TAB = "\u255f ";
    private static final String PRINT_FOLDER_TAB = "\u2560 ";
    private static final int COLUMN_LEFT_WIDTH = 100;
    private static final int COLUMN_RIGHT_WIDTH = 25;
    private static final int TOTAL_WIDTH = COLUMN_LEFT_WIDTH + COLUMN_RIGHT_WIDTH + UNITS.length();
    private static final int KILO = 1024;


    public String format(Path currentPath) {
        StringBuilder stringBuilder = new StringBuilder();
        int tab = calcTab(currentPath);
        String tabulator = "-".repeat(tab);
        stringBuilder
                .append(tabulator)
                .append(" ")
                .append(currentPath.getFileName());

        return stringBuilder.toString();
    }

    private int calcTab(Path currentPath) {
        // todo calculate difference between paths   beginPath.length - currentPath.length
        return currentPath.getNameCount() - beginPath.getNameCount();
    }



//    private static void printSum() {
//        System.out.print("-".repeat(TOTAL_WIDTH));
//        System.out.println();
//
//        String size = switch (Long.toString(length).length()) {
//            case 1, 2, 3 -> " bytes";
//            case 4, 5, 6 -> (double) Math.round(length / Math.pow(KILO, 1) * 10) / 10 + " kb";
//            case 7, 8, 9 -> (double) Math.round(length / Math.pow(KILO, 2) * 10) / 10 + " Mb";
//            default -> (double) Math.round(length / Math.pow(KILO, 3) * 10) / 10 + " Gb";
//        };
//
//        String format = "%" + TOTAL_WIDTH + "s";
//        System.out.printf(format, "TOTAL SIZE: " + size);
//    }


//
//
//        try {
//            if (f.isDirectory()) {
//                printTab(tabulator++);
//                int filler = COLUMN_LEFT_WIDTH - tabulator * PRINT_FOLDER_TAB.length();
//                String format = PRINT_FOLDER_TAB + "%-" + filler + "s";
//                System.out.printf(format, f.getName().toUpperCase());
//                String format2 = "%" + (COLUMN_RIGHT_WIDTH + FOLDER.length() - PRINT_FOLDER_TAB.length()) + "s%n";
//                System.out.printf(format2, FOLDER);
//                printFileListRow(f, Loader.printFileAttribute);
//            } else {
//                printTab(tabulator);
//                int filler = COLUMN_LEFT_WIDTH - tabulator * PRINT_FILE_TAB.length();
//                String format = "%-" + filler + "s";
//                System.out.printf(format, f.getName());
//                String format2 = "%," + COLUMN_RIGHT_WIDTH + "d" + UNITS + "%n";
//                System.out.printf(format2, f.length());
//                length += f.length();
//            }
//        } catch (NullPointerException npe) {
//            // As Javadoc for File.listFiles() states:
//            // Returns null if this abstract pathname does not denote a directory, or if an I/O error occurs.
//
//            printTab(tabulator + 1);
//            int filler = COLUMN_LEFT_WIDTH - tabulator * PRINT_FOLDER_TAB.length();
//            String message = "*** You don't currently have permission to access this folder ***";
//            String format = "%-" + filler + "s %n";
//            System.out.printf(format, message);
//        }

    //    private static void printFileListRow(File dir, Consumer<File> file) {
//        Arrays.stream(dir.listFiles()).forEach(file);
//        tabulator--;
//    }
//


//


}
