package DirectoryScanner.ListFormatters;

import java.nio.file.Path;

abstract public class ListFormatter {
    protected Path beginPath;

    abstract public String format(Path currentPath);

    public void setBeginPath(Path beginPath) {
        this.beginPath = beginPath;
    }

    public Path getBeginPath() {
        return beginPath;
    }
}
