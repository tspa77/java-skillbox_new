package DirectoryScanner;

import DirectoryScanner.Exporters.Exporter;
import DirectoryScanner.ListFormatters.ListFormatter;
import DirectoryScanner.Util.UserPath;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DirectoryScanner {
    private Exporter exporter;
    private ListFormatter listFormatter;

    public DirectoryScanner(Exporter exporter, ListFormatter listFormatter) {
        this.exporter = exporter;
        this.listFormatter = listFormatter;
    }

    public void run() {
        // get path from user
        Path path = UserPath.request();

        // init scanner
        listFormatter.setBeginPath(path);
        exporter.setListFormatter(listFormatter);

        //run
        try {
            Files.walkFileTree(path, exporter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}