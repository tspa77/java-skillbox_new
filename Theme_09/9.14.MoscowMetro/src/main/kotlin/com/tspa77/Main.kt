package com.tspa77

import com.tspa77.mmparser.MoscowMetroParser

/**
 * Написать код парсинга страницы Википедии “Список станций Московского метрополитена”,
 * который будет на основе этой страницы создавать JSON-файл со списком станций по
 * линиям и списком линий по формату JSON-файла из проекта SPBMetro
 */

fun main() {
    MoscowMetroParser().run()
}

