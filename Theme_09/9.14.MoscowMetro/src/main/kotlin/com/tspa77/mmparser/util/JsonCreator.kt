package com.tspa77.mmparser.util

import com.tspa77.mmparser.model.parse.Connection
import com.tspa77.mmparser.model.parse.Line
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import java.nio.file.Files
import java.nio.file.Paths

class JsonCreator(
    private val setLines: Set<Line>,
    private val setConnection: Set<Connection>,
    private val jsonFilePath: String
) {
    private val objJSON = JSONObject()

    fun run() {
        assembleObject()
        saveToFile()
    }

    private fun assembleObject() {
        val objStations = JSONObject()
        val arrConnections = JSONArray()
        val arrLines = JSONArray()

        // stations
        setLines.forEach {
            val arrStationInLine = JSONArray()
            arrStationInLine.addAll(it.stations.map { s -> s.name })
            objStations[it.number] = arrStationInLine
        }

        // connections
        setConnection.forEach { itC ->
            val arrConnection = JSONArray()
            itC.listPairs.forEach { itP ->
                val objPoint = JSONObject()
                objPoint["line"] = itP.first
                objPoint["station"] = itP.second
                arrConnection.add(objPoint)
            }
            arrConnections.add(arrConnection)
        }

        // lines
        setLines.forEach {
            val objLine = JSONObject()
            objLine["number"] = it.number
            objLine["name"] = it.name
            objLine["color"] = it.color
            arrLines.add(objLine)
        }

        objJSON["stations"] = objStations
        objJSON["connections"] = arrConnections
        objJSON["lines"] = arrLines
    }

    private fun saveToFile() =
        Files.write(Paths.get(jsonFilePath), objJSON.toJSONString().toByteArray());
}