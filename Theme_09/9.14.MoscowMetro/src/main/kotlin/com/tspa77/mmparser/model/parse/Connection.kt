package com.tspa77.mmparser.model.parse

data class Connection(val listPairs: List<Pair<String, String>>)