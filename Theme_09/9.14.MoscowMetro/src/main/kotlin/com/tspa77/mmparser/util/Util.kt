package com.tspa77.mmparser.util

import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Files
import java.nio.file.InvalidPathException
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

/**
 * Метод возвращает содержимое странице в ввиде текста
 */


/**
 * Метод запрашивает у пользователя папку для сохранения. Если папки не существует, она будет создана
 */
fun userPathRequest(message: String): Path {
    val scanner = Scanner(System.`in`)
    var path: Path

    while (true) {
        println(message)
        val string = scanner.nextLine().trim { it <= ' ' }
        try {
            path = Paths.get(string)
            if (!Files.exists(path)) {
                try {
                    Files.createDirectories(path)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            break
        } catch (e: InvalidPathException) {
            println("$string - некорректный путь")
        }

    }
    return path
}