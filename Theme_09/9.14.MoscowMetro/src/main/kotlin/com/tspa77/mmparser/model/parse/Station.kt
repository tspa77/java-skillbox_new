package com.tspa77.mmparser.model.parse

data class Station(val name: String, val linesNumbers: List<String>, val listConnection: List<Pair<String,String>>)

