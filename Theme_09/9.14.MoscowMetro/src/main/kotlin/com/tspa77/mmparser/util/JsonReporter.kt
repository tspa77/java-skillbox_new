package com.tspa77.mmparser.util

import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.nio.file.Files
import java.nio.file.Paths

class JsonReporter(private val jsonFilePath: String) {
    private val linesArray = JSONArray()
    private var stationsObject = JSONObject()
    private val connectionsArray = JSONArray()

    fun run() {
        val objJSON = getJSONObject()
        parseJSONObject(objJSON)
        val mapLinesAndStation = parseStationInLines(stationsObject)
        val setLines = parseLines(linesArray)
        printStationReport(mapLinesAndStation, setLines)
    }

    private fun loadFile() = Files.readAllBytes(Paths.get(jsonFilePath))

    private fun getJSONObject(): JSONObject {
        val jsonParser = JSONParser()
        return jsonParser.parse(String(loadFile())) as JSONObject
    }

    private fun parseJSONObject(objJSON: JSONObject) {
        linesArray.addAll(objJSON["lines"] as JSONArray)
        stationsObject = objJSON["stations"] as JSONObject
        connectionsArray.addAll(objJSON["connections"] as JSONArray)
    }

    private fun parseStationInLines(stationsObject: JSONObject): MutableMap<String, List<String>> {
        val mapLinesAndStation = mutableMapOf<String, List<String>>()
        stationsObject.keys.forEach { lineNumberObject ->
            val lineNumber = lineNumberObject as String
            val stationsArray = stationsObject[lineNumberObject] as JSONArray
            val stationInLine = stationsArray.map { s -> s.toString() }
            mapLinesAndStation[lineNumber] = stationInLine
        }
        return mapLinesAndStation
    }

    private fun parseLines(linesArray: JSONArray): MutableSet<Triple<String, String, String>> {
        val setLines = mutableSetOf<Triple<String, String, String>>()
        linesArray.forEach {
            val itemObject = it as JSONObject
            val number = itemObject["number"] as String
            val color = itemObject["color"] as String
            val name = itemObject["name"] as String
            setLines.add(Triple(number, color, name))
        }
        return setLines
    }

    private fun printStationReport(
        mapLinesAndStation: MutableMap<String, List<String>>,
        lines: MutableSet<Triple<String, String, String>>
    ) {
        mapLinesAndStation.forEach { itM ->
            val requiredItem = lines.first { itL ->
                itL.first == itM.key
            }
            println("${requiredItem.third}  -  ${itM.value.size} станций")
        }
    }
}