package com.tspa77.mmparser

import com.tspa77.mmparser.model.parse.Connection
import com.tspa77.mmparser.model.parse.Line
import com.tspa77.mmparser.model.parse.ParseObject
import com.tspa77.mmparser.model.parse.Station
import com.tspa77.mmparser.util.*
import org.jsoup.Jsoup
import java.net.URL


class MoscowMetroParser {
    // Не стал получать адрес тстраницы входным параметром, т.к. заточено всё под одну
    // конкретную страницу и больше ни с чем работать не сможет всё равно.
    companion object {
        private const val urlMM =
            "https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%81%D1%82%D0%B0%D0%BD%D1%86%D0%B8%D0%B9_%D0%9C%D0%BE%D1%81%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B3%D0%BE_%D0%BC%D0%B5%D1%82%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D0%B8%D1%82%D0%B5%D0%BD%D0%B0"
        private const val jsonFilePath = "data/map.json"
    }

    fun run() {
        val text = parseUrl(URL(urlMM))
        val doc = Jsoup.parse(text)
        val elements = doc.select("table")

        // Получаем таблицу метро
        val tableMetroRaw = elements[3].select("tbody").select("tr")
        // Отбрасываем первый элемент - заголовки
        val tableStations = tableMetroRaw.drop(1)
        // Парсим на объекты. Деление на станции и линии будет позже
        val listParseObjects = parseObjects(tableStations)

        // Создаём линии
        val setLines = createLines(listParseObjects)
        // Создаём станции
        val setStations = createStations(listParseObjects, setLines)

        // Заполняем линии станциями
        setLines.forEach { itL ->
            itL.stations.addAll(setStations.filter { itS -> itL.number in itS.linesNumbers })
        }

        // Создаём переходы
        val setConnections = createConnections(setLines)

        // Создаём и сохраняем JSON
        val jsonCreator = JsonCreator(setLines, setConnections, jsonFilePath)
        jsonCreator.run()

        // Парсим сохранённый JSON и печтаем требуемый отчёт
        val jsonReporter = JsonReporter(jsonFilePath)
        jsonReporter.run()
    }

    private fun createLines(listParseObjects: List<ParseObject>): Set<Line> {
        val setLine = mutableSetOf<Line>()
        listParseObjects.forEach {
            setLine.add(
                Line(
                    number = it.linesNumbers[0],
                    name = it.lineName,
                    color = it.lineColor
                )
            )
        }
        return setLine
    }

    private fun createStations(listParseObjects: List<ParseObject>, setLines: Set<Line>): Set<Station> {
        val setStation = mutableSetOf<Station>()
        listParseObjects.forEach { itPO ->
            val connections = mutableListOf<Pair<String, String>>()
            itPO.connections.forEach { itCN ->
                connections.add(
                    parseStationNameInConnection(itCN, setLines)
                )
            }
            setStation.add(Station(itPO.stationName, itPO.linesNumbers, connections.toList()))
        }
        return setStation
    }

    private fun createConnections(setLines: Set<Line>): Set<Connection> {
        val setConnection = mutableSetOf<Connection>()
        setLines.forEach { itL ->
            itL.stations.filter { itS ->
                itS.listConnection.isNotEmpty()
            }.forEach {
                val listPair = mutableListOf<Pair<String, String>>()
                listPair.add(itL.number to it.name)
                listPair.addAll(it.listConnection)
                listPair.sortBy { pair -> pair.first }
                setConnection.add(Connection(listPair))
            }
        }
        return setConnection
    }
}

