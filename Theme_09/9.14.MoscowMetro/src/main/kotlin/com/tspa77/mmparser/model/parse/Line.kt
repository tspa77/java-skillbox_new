package com.tspa77.mmparser.model.parse

data class Line(val number: String, val name: String, val color: String) {
    val stations = mutableListOf<Station>()
}
