package com.tspa77.mmparser.model.parse

data class ParseObject(
    val linesNumbers: List<String>,
    val lineName: String,
    val lineColor: String,
    val stationName: String,
    val connections: List<Pair<String, String>>
)