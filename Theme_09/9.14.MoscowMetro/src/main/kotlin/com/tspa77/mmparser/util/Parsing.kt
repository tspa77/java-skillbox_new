package com.tspa77.mmparser.util

import com.tspa77.mmparser.model.parse.Line
import com.tspa77.mmparser.model.parse.ParseObject
import org.jsoup.nodes.Element
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Отладочный метод, возвращает содержимое локальной страниц в ввиде текста
 */
fun parseFile(path: String): String {
    val stringBuilder = StringBuilder()
    val lines = Files.readAllLines(Paths.get(path))
    lines.forEach { it -> stringBuilder.append(it + "\n") }
    return stringBuilder.toString()
}

/**
 * Метод возвращает содержимое web-страницы в ввиде текста
 */
fun parseUrl(url: URL): String {
    val urlConnection = url.openConnection() as HttpURLConnection
    val text = urlConnection.inputStream.bufferedReader().readText()
    urlConnection.disconnect()
    return text
}

/**
 * Метод возвращает объект содержащий в себе информацию общую информацию о станции, линии и пересадках
 */
fun parseObjects(tableStations: List<Element>): List<ParseObject> {
    // Список для результатов парсинга.
    val listParseObjects = mutableListOf<ParseObject>()
    // Парсинг. Жестокий и беспощадный.
    tableStations.forEach { it ->
        val stationRaw = it.select("td")
        // Т.к. станции могут находиться в двух ветках сарзу парсим имена веток в лист
        val linesNumbersRaw = stationRaw[0].select("span").text().trim().split("  ").dropLast(1)
        val linesNumbers = removeZeroFromBeginName(linesNumbersRaw)
        val lineName = stationRaw[0].child(1).getElementsByTag("span")[0].attributes().get("title")
        val lineColorRaw = stationRaw[0].attributes().get("style")
        val lineColor = Regex("""#\w{6}""").find(lineColorRaw)?.value.orEmpty()
        val stationName = stationRaw[1].select("a").first().html()
        val connectionsStationNameRaw = stationRaw[3].children().tagName("span").eachAttr("title")
        val connectionsNumbersLineRaw = stationRaw[3].select("span").text().trim().split("  ")
        val connectionsNumbersLine = removeZeroFromBeginName(connectionsNumbersLineRaw)
        // Создаём переходы для текущей станции
        val connections = mutableListOf<Pair<String, String>>()
        for (i in connectionsStationNameRaw.indices) {
            connections.add(connectionsNumbersLine[i] to connectionsStationNameRaw[i])
        }
        // Создаём объект
        listParseObjects.add(
            ParseObject(
                linesNumbers,
                lineName,
                lineColor,
                stationName,
                connections
            )
        )
    }
    return listParseObjects
}

/**
 *  Для того чтобы вытащить название станции из таких строк
 *  "Переход на станцию Савёловская Серпуховско-Тимирязевской линии"
 *  "Кросс-платформенная пересадка на станцию Каширская Замоскворецкой линии"
 */
fun parseStationNameInConnection(connectionRaw: Pair<String, String>, setLines: Set<Line>): Pair<String, String> {

    // Костыль. 13-я и 14-я линция в основной таблице отсутствует, но они участвует в переходах.
    // Для правильного парсинга имён добавялю их руками :(
    val setLinesFull = setLines.toMutableSet()
    setLinesFull.add(Line("13", "Московский монорельс", "#000000"))
    setLinesFull.add(Line("14", "Московское центральное кольцо", "#FFFFFF"))

    // фиксированая левая граница для regex (?<=$LEFT_BORDER).+?(?= $rightBorder)
    val LEFT_BORDER = "станцию "

    // Для правой границы получаем имя Линии по её номеру (он известен, парсится,
    // лежит первым в паре connection)
    val rightBorder = setLinesFull
        .first { it.number == connectionRaw.first }
        .name
        .split(" ")
        .first()                       // Отрезали имя
        .dropLast(3)                // Отрезали у имени окончание, т.к. исходная в родительном падеже

    // Парсим строку по границам
    val matchResult =
        Regex("(?<=$LEFT_BORDER).+?(?= $rightBorder)").find(connectionRaw.second)

    // Забираем середину
    val station = matchResult!!.value

    return Pair(connectionRaw.first, station)
}

fun removeZeroFromBeginName(nameRawList: List<String>): List<String> {
    // тут наверное как-то красиво через стрим можно, но я не придумал :(
    val nameList = mutableListOf<String>()
    nameRawList.forEach {
        if (it.isNotBlank()) {
            nameList.add(
                if (it.first() == '0') {
                    it.drop(1)
                } else {
                    it
                }
            )
        }
    }
    return nameList
}