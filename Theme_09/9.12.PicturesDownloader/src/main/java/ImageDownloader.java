import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class ImageDownloader {
    private Path targetPath;
    private String sourceUrl;

    public ImageDownloader(Path targetPath, String sourceUrl) {
        this.targetPath = targetPath;
        this.sourceUrl = sourceUrl;
    }

    void run() throws IOException {
        System.out.println("Please wait...");

        Document doc = Jsoup.connect(sourceUrl).get();
        Elements images = doc.getElementsByTag("img");

        for (Element image : images) {
            // Получаем имя файла
            String absUrl = image.absUrl("src");

            String imageName = absUrl.substring(absUrl.lastIndexOf("/") + 1);
            if (isFileNameCorrect(imageName) && isImageFile(imageName)) {
                // Сохраняем файл
                InputStream inputStream = new URL(image.absUrl("src")).openStream();
                Files.copy(inputStream, Paths.get(targetPath + File.separator + imageName), REPLACE_EXISTING);
            }
        }
        System.out.println("Mission complete");
    }

    private static boolean isFileNameCorrect(String name) {
        Pattern pattern = Pattern.compile("(.+)?[><\\|\\?*/:\\\\\"](.+)?");
        Matcher matcher = pattern.matcher(name);
        return !matcher.find();
    }

    private static boolean isImageFile(String name) {
        Pattern pattern = Pattern.compile("([^s]+(.(?i)(jpg|png|gif|bmp|tiff))$)");
        Matcher matcher = pattern.matcher(name);
        return matcher.find();
    }
}

