import FileHelper.FileHelper;
import FileHelper.Util.UserPath.CheckAvailability;

import java.nio.file.Path;

import static FileHelper.Util.UserPath.request;

public class Main {
    public static void main(String[] args) {

        Path sourceDirectory = request(
                "Введите путь к исходной папке", CheckAvailability.YES);
        Path targetDirectory = request(
                "Введите путь к папке назначения", CheckAvailability.NO);

        var fileHelper = new FileHelper();

        fileHelper.copy(sourceDirectory, targetDirectory);

    }
}
