package FileHelper.Action;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.util.ArrayList;

public abstract class BaseAction extends SimpleFileVisitor<Path> {
    Path sourceDirectory;
    Path targetDirectory;
    ArrayList<Path> listErrors;
    int folder;
    int files;
    long size;

    public abstract void printResult();

    public void start() {
        try {
            Files.walkFileTree(sourceDirectory, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.printResult();
    }

}
