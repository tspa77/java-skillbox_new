package FileHelper.Action;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

import static FileHelper.Util.PathHelper.*;
import static FileHelper.Util.PathHelper.getTargetPath;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class Copy extends BaseAction {

    public Copy(Path sourceDirectory, Path targetDirectory) {
        this.sourceDirectory = sourceDirectory;
        this.targetDirectory = targetDirectory.resolve(sourceDirectory.getFileName());
        listErrors = new ArrayList<>();
    }


    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        Path path = getTargetPath(
                dir,
                sourceDirectory,
                targetDirectory);
        if (!Files.exists(path)) {
            try {
            Files.createDirectories(path);
            folder++;
            } catch (IOException e) {
                e.printStackTrace();
                return FileVisitResult.SKIP_SUBTREE;
            }
        }
        return FileVisitResult.CONTINUE;
    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        Path path = getTargetPath(
                file,
                sourceDirectory,
                targetDirectory);
        try {
            Files.copy(file, path, REPLACE_EXISTING);
            files++;
            size += Files.size(file);
        } catch (IOException e) {
            e.printStackTrace();
            return FileVisitResult.SKIP_SUBTREE;
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        listErrors.add(file);
        return FileVisitResult.SKIP_SUBTREE;
    }

    @Override
    public void printResult() {
        System.out.print("Копирование выполнено");
        if (!listErrors.isEmpty()) {
            System.out.print(" относительно");
        }
        System.out.printf(" успешно.%nСкопировано %d папок и %d файлов. " +
                "Общий объём %s%n", folder, files, formatSum(size));
        if (!listErrors.isEmpty()) {
            System.out.printf("Не удалось скопировать папок/файлов: %d шт.%n",
                    listErrors.size());
            listErrors.forEach(System.out::println);
        }
    }

}
