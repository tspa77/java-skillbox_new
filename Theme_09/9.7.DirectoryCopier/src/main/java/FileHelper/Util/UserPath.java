package FileHelper.Util;


import java.nio.file.*;
import java.util.Scanner;

public class UserPath {

    public enum CheckAvailability {YES, NO}

    public static Path request(String message, CheckAvailability checkAvailability) {
        var scanner = new Scanner(System.in);
        Path path;

        while (true) {
            System.out.println(message);
            String string = scanner.nextLine().trim();
            try {
                path = Paths.get(string);
                if (checkAvailability.equals(CheckAvailability.YES)) {
                    if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
                        break;
                    }
                    System.out.println(path + " - пути не существует");
                } else {
                    break;
                }

            } catch (InvalidPathException e) {
                System.out.println(string + " - некорректный путь");
            }
        }
        return path;
    }

}