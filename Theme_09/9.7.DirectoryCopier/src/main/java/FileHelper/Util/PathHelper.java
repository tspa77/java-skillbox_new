package FileHelper.Util;

import java.nio.file.Path;

public class PathHelper {
    private static final int KILO = 1024;

    public static Path getTargetPath(Path currentPath, Path sourceDirectory, Path targetDirectory) {
        return targetDirectory.resolve(sourceDirectory.relativize(currentPath));
    }


    public static String formatSum(long sum) {
        return switch (Long.toString(sum).length()) {
            case 1, 2, 3 -> sum + " bytes";
            case 4, 5, 6 -> (double) Math.round(sum / Math.pow(KILO, 1) * 10) / 10 + " kb";
            case 7, 8, 9 -> (double) Math.round(sum / Math.pow(KILO, 2) * 10) / 10 + " Mb";
            default -> (double) Math.round(sum / Math.pow(KILO, 3) * 10) / 10 + " Gb";
        };
    }
}
