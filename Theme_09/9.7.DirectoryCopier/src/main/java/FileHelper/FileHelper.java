package FileHelper;

import FileHelper.Action.BaseAction;
import FileHelper.Action.Copy;
import FileHelper.Action.Delete;
import FileHelper.Action.Move;

import java.nio.file.Path;

public class FileHelper {

    public void copy(Path sourceDirectory, Path targetDirectory) {
        BaseAction action = new Copy(sourceDirectory, targetDirectory);
        action.start();
    }

    public void move(Path sourceDirectory, Path targetDirectory) {
        BaseAction action = new Move(sourceDirectory, targetDirectory);
        action.start();
    }

    public void delete(Path targetDirectory) {
        BaseAction action = new Delete(targetDirectory);
        action.start();
    }
}
