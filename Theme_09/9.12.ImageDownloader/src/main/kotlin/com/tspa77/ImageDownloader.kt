package com.tspa77

import com.tspa77.util.getImagesUrl
import com.tspa77.util.parseUrl
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption.REPLACE_EXISTING

class ImageDownloader(siteAddress: String, private val path: Path) {
    private val url = URL(siteAddress)

    fun run() {
        val text = parseUrl(url)
        val listImageUrl = getImagesUrl(text)
        download(listImageUrl)
    }

    private fun download(listImageUrl: List<String>) {
        listImageUrl.forEach {
            val imageName = it.split("""/""").last()
            val fullPathFile = path.resolve(imageName)

            URL(it).openStream().use { stream ->
                Files.copy(stream, fullPathFile, REPLACE_EXISTING)
            }
        }
    }


}