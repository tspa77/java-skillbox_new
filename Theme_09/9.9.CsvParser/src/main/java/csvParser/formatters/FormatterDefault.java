package csvParser.formatters;

import csvParser.models.Transaction;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FormatterDefault {
    private static int PRINT_WIDTH = 107;            // Даже идея говорит - делай магию :)
    private List<Transaction> transactionList;
//    private Set<String> counterAgent;

    private static class CashFlow {
        private double income;                      //	Приход
        private double withdraw;                     //	Расход

        CashFlow() {
        }

        CashFlow(double income, double withdraw) {
            this.income = income;
            this.withdraw = withdraw;
        }

        static CashFlow merge(CashFlow cf1, CashFlow cf2) {
            return new CashFlow(cf1.income + cf2.income, cf1.withdraw + cf2.withdraw);
        }

        static CashFlow fromTransaction(Transaction t) {
            return new CashFlow(t.getIncome(), t.getWithdraw());
        }
    }

    public FormatterDefault(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public void print() {
        printStream();

//        fillCounterAgentSet();
//        counterAgent.forEach(this::printCounterAgentReport);
//        printTotal(calcCashFlow(transactionList));
    }

    private void printTotal(CashFlow cashFlow) {
        System.out.println("-".repeat(PRINT_WIDTH));
        printRow("ИТОГО", cashFlow);
    }

    private void printStream() {
        transactionList.stream()
                .collect(Collectors.groupingBy(Transaction::getCounterAgent,
                        Collectors.mapping(CashFlow::fromTransaction,
                                Collectors.reducing(new CashFlow(), CashFlow::merge)
                        )))
                .forEach(this::printRow);

        printTotal(transactionList.stream()
                .map(CashFlow::fromTransaction)
                .reduce(new CashFlow(), CashFlow::merge));
    }

    private void printRow(String name, CashFlow cashFlow) {
        System.out.printf("%-55s Приход %,15.2f; \t Расход %,15.2f%n",
                name, cashFlow.income, cashFlow.withdraw);
    }

//    private void fillCounterAgentSet() {
//        counterAgent = transactionList
//                .stream()
//                .map(Transaction::getCounterAgent)
//                .collect(Collectors.toSet());
//    }

//    private void printCounterAgentReport(String cAgent) {
//        List<Transaction> transactionAgentList = transactionList.stream()
//                .filter(x -> x.getCounterAgent().equals(cAgent))
//                .collect(Collectors.toList());
//        var cashFlow = calcCashFlow(transactionAgentList);
//        printRow(cAgent, cashFlow);
//    }

//    private CashFlow calcCashFlow(List<Transaction> transactionList) {
//        var cashFlow = new CashFlow();
//        for (Transaction transaction : transactionList) {
//            cashFlow.withdraw += transaction.getWithdraw();
//            cashFlow.income += transaction.getIncome();
//        }
//        return cashFlow;
//    }
}
