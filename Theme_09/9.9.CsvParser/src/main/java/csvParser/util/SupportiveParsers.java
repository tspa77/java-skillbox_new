package csvParser.util;

import java.time.format.DateTimeFormatter;

public class SupportiveParsers {
    private static final int COLUMN = 8;
    private static final int COLUMN_START = 0;
    private static final int COLUMN_A = 20;
    private static final int COLUMN_COUNTER_AGENT = 70;
    private static final int COLUMN_DATE1 = 79;
    private static final int COLUMN_DATE2 = 88;
    private static final int COLUMN_SUM = 102;
    private static final int COLUMN_CURRENCY = 106;

    public static Double parseDouble(String string) {
        return Double.parseDouble(string
                .replaceAll("^\"|\"$", "")
                .replace(",", "."));
    }


    public static String[] parseDescription(String string) {

        final int[] restriction = {COLUMN_START, COLUMN_A, COLUMN_COUNTER_AGENT,
                COLUMN_DATE1, COLUMN_DATE2, COLUMN_SUM, COLUMN_CURRENCY, string.length()};

        final String[] description = new String[COLUMN];

        // Магическая двойка, т.к. последние 2 столбца обрабатываются после цикла
        for (int i = 0; i < COLUMN - 2; i++) {
            description[i] = string.substring(restriction[i], restriction[i + 1]).strip();
        }

        var residue = string.substring(COLUMN_CURRENCY).strip().split("MCC");
        description[6] = residue[0].strip();
        description[7] = "MCC" + residue[1].strip();

        return description;
    }

    public static String[] parsingRow(String string) {
        return string.split("(,)(?=(?:[^\"]|\"[^\"]*\")*$)");
    }

    public static DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("dd.MM.yy");
}
