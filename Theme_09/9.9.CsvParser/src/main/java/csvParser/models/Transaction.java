package csvParser.models;

import java.time.LocalDate;

import static csvParser.util.SupportiveParsers.*;

public class Transaction {
    private String accountType;                 //	Тип счёта
    private String accountNumber;               //	Номер счета
    private String currency;                    //	Валюта
    private LocalDate date;                     //	Дата операции
    private String referencePosting;            //	Референс проводки
    private Description description;            //	Описание операции
    private String counterAgent;                //  Контрагент
    private double income;                      //	Приход
    private double withdraw;                    //	Расход

    static class Description {
        String someValue;
        String counterAgent;
        LocalDate date1;
        LocalDate date2;
        double sum;
        String currency;
        String anotherValue;
        String mcc;

        Description(String[] row) {
            this.someValue = row[0];
            this.counterAgent = row[1];
            this.date1 = LocalDate.parse(row[2], formatter);
            this.date2 = LocalDate.parse(row[3], formatter);
            this.sum = parseDouble(row[4]);
            this.currency = row[5];
            this.anotherValue = row[6];
            this.mcc = row[7];
        }
    }


    public static Transaction getInstanceFromRow(String[] row) {
        Transaction transaction = new Transaction();
        transaction.accountType = row[0];
        transaction.accountNumber = row[1];
        transaction.currency = row[2];
        transaction.date = LocalDate.parse(row[3], formatter);
        transaction.referencePosting = row[4];
        transaction.description = new Description(parseDescription(row[5]));
        transaction.counterAgent = transaction.description.counterAgent;
        transaction.income = parseDouble(row[6]);
        transaction.withdraw = parseDouble(row[7]);
        return transaction;
    }

    public Double getIncome() {
        return income;
    }

    public Double getWithdraw() {
        return withdraw;
    }

    public String getCounterAgent() {
        return counterAgent;
    }
}
