package csvParser.readers;

import java.util.List;

public interface CsvReader {
    List<String> read();
}
