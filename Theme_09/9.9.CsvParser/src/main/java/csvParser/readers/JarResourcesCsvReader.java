package csvParser.readers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

public class JarResourcesCsvReader implements CsvReader {
    private String fileName;

    public JarResourcesCsvReader(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public List<String> read() {
        Path path;
        List<String> list;
        try {
            path = Paths.get(Objects.requireNonNull(getClass()
                    .getClassLoader()
                    .getResource(fileName))
                    .toURI());
            list = Files.readAllLines(path);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return list;
    }
}
