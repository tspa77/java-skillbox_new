package csvParser;

import csvParser.models.Transaction;
import csvParser.readers.CsvReader;
import csvParser.util.SupportiveParsers;

import java.util.List;
import java.util.stream.Collectors;

public class Parser {

    public static List<Transaction> getTransactionList(CsvReader reader) {

        List<String> list = reader.read();
        List<String> cleanList = removeHeader(list);
        return cleanList.stream()
                .map(SupportiveParsers::parsingRow)
                .map(Transaction::getInstanceFromRow)
                .collect(Collectors.toList());
    }

    private static List<String> removeHeader(List<String> list) {
        if (!list.isEmpty()) {
            list.remove(0);
        }
        return list;
    }

}
