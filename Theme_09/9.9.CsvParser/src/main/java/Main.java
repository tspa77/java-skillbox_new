import csvParser.Parser;
import csvParser.formatters.FormatterDefault;
import csvParser.models.Transaction;
import csvParser.readers.JarResourcesCsvReader;

import java.util.List;

/**
 * ДР 9.9
 * Написать код парсинга банковской выписки (файл movementsList.csv).
 * Код должен выводить сводную информацию по этой выписке: общий приход, общий расход, а также разбивку расходов.
 */

public class Main {
    public static void main(String[] args) {

        var resReader = new JarResourcesCsvReader("movementList.csv");
        List<Transaction> transactionList =
                Parser.getTransactionList(resReader);

        var formatterDefault = new FormatterDefault(transactionList);
        formatterDefault.print();

    }
}
