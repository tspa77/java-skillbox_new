
public class Loader {
    private final static char STRING_SEPARATOR = ',';
    private final static String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000";


    public static void main(String[] args) {
        System.out.println(text);

//        String[] array = text.split(",");
//        int sum = 0;
//
//        for (String s : array) {
//            sum += Integer.parseInt(s.replaceAll("\\D", ""));
//        }

        int sum = 0;
        int start = 0;
        int end = text.indexOf(STRING_SEPARATOR);

        while (true) {
            sum += numberExtractor(start, end);

            // если это ещё не конец был, то обновляем разметку нового старта
            if (end != text.length()) {
                start = end + 1;
            } else {
                // или выходим из цикла
                break;
            }

            if (text.substring(start).indexOf(STRING_SEPARATOR) != -1) {
                // если запятые ещё есть то объявляем концом блока позицию следующей запятой
                end = start + text.substring(start).indexOf(STRING_SEPARATOR);
            } else {
                // иначе это последний блок и конец блока это конец всего текста
                end = text.length();
            }
        }
        System.out.println("Итого: " + sum);
    }


    private static int numberExtractor(int startFragment, int endFragment) {
        int number = 0;
        String fragment = text.substring(startFragment, endFragment);
//        System.out.println(fragment);
        for (int i = 0; i < fragment.length(); i++) {
            if (Character.isDigit(fragment.charAt(i))) {
                number *= 10;
                number += Integer.parseInt(fragment.substring(i, i + 1));
            }
        }
        return number;
    }
}