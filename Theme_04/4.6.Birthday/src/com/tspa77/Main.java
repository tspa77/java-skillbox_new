package com.tspa77;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;

public class Main {

    public static void main(String[] args) {
        // java.util.*
        Calendar birthday = Calendar.getInstance();
        birthday.set(1977, Calendar.FEBRUARY, 10);
        Calendar now = Calendar.getInstance();
        int count = 0;

        DateFormat df = new SimpleDateFormat(" - dd.MM.yyyy - EEE", Locale.US);

        while (birthday.getTime().before(now.getTime())) {
            System.out.println(count + df.format(birthday.getTime()));
            birthday.roll(Calendar.YEAR, 1);
            count++;
        }


        System.out.println("\n\n ******************** \n\n");
        // java.time.*

        LocalDate now8 = LocalDate.now();
        LocalDate birthday8 = LocalDate.of(1977, Month.FEBRUARY, 10);
        count = 0;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(" - dd.MM.yyyy - EEE", Locale.US);

        while (birthday8.isBefore(now8)) {
            String stringBirthday8 = birthday8.format(formatter);
            System.out.println(count + stringBirthday8);
            birthday8 = birthday8.plusYears(1);
            count++;
        }

    }
}
