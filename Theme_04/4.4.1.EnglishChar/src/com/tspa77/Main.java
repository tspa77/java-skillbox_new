package com.tspa77;

public class Main {

    public static void main(String[] args) {

        char ch;
        for (int i = 0; i < 65536; i++) {
            ch = (char) i;
            if (String.valueOf(ch).matches("[A-Za-z]")) {
                System.out.println(i + " соотвествует " + ch);
            }
        }
    }
}
