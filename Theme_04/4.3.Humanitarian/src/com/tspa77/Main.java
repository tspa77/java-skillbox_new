package com.tspa77;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {

        int container = 27;
        int truck = 12 * container;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число ящиков: ");
        int box = Integer.parseInt(reader.readLine());

        for (int i = 0; i < box; i++) {
            if ((i + truck) % truck == 0)
                System.out.println("Грузовик " + (i / truck + 1) + ": ");
            if ((i + container) % container == 0)
                System.out.println("\tКонтейнер " + (i / container + 1) + ": ");
            System.out.println("\t\t\tЯщик " + (i + 1));
        }
    }
}
