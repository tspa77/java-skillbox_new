package com.tspa77;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number = reader.readLine().replaceAll("\\D", "");
        if (number.length() != 11) {
            System.out.println("Вы что-то с длинной напутали. Должно быть 11 цифр");
        } else {
            System.out.println(number);
        }
    }
}
