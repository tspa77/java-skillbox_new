package com.tspa77;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static final String RUSSIAN_NAME = "[А-ЯЁ&&[^ЪЬЫ]]{1}[а-яё]+\\s";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fio = reader.readLine().trim();

        String fullNameTemplate = RUSSIAN_NAME.repeat(2) + RUSSIAN_NAME.substring(0, 24) + "$";

        if (fio.matches(fullNameTemplate)) {
            String[] array = fio.split("\\s+");
            System.out.println("Фамилия: " + array[0]);
            System.out.println("Имя: " + array[1]);
            System.out.println("Отчество: " + array[2]);
        } else {
            System.out.println("Некорректный ввод. Все элементы ФИО (фамилия, " +
                    "имя, отчество)должны состоять из букв  русского алфавита: " +
                    "первая заглавная, остальные маленькие.");
        }
    }
}
