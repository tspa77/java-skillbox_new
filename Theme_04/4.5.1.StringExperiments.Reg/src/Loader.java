
public class Loader {
    private final static String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000";


    public static void main(String[] args) {
        System.out.println(text);

        String[] array = text.split(",");
        int sum = 0;

        for (String s : array) {
            sum += Integer.parseInt(s.replaceAll("\\D", ""));
        }
        System.out.println("Итого: " + sum);
    }
}