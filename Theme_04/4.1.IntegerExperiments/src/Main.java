public class Main {
    public static void main(String[] args) {
        Container container = new Container();
        container.count += 7843;

        System.out.println(sumDigits(123456));
    }

    public static Integer sumDigits(Integer number) {
        //@TODO: write code here

        String string = Integer.toString(number);
        int sum = 0;

        for (int i = 0; i < string.length(); i++) {
            sum += Character.getNumericValue(string.charAt(i));
        }

        return sum;
    }
}
