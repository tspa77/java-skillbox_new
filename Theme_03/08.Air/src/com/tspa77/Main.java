package com.tspa77;

import com.skillbox.airport.Aircraft;
import com.skillbox.airport.Airport;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Airport airport = Airport.getInstance();
        List<Aircraft> aircraftList = airport.getAllAircrafts();

        System.out.println("Всего в аэропорту воздушных транспортных средств: "
                + aircraftList.size() + ". Полный список:");

        for (Aircraft aircraft : aircraftList) {
            System.out.println(aircraft.getModel());
        }
    }
}
