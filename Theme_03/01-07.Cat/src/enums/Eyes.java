package enums;

public enum Eyes {

    WITHOUT_EYES(0),
    ONE(1),
    TWO(2);

    int eyes;


    Eyes(int eyes) {
        this.eyes = eyes;
    }

    public int eyes(){
        return eyes;
    }
}
