import enums.Eyes;

import java.util.*;

public class Cat {

    private LiveStatus liveStatus;
    private String name;
    private int number;
    private double originWeight;
    private double weight;
    private Eyes eyes;
    private double lastMeal;    // Урок 2. Создать в классе Cat метод, который будет возвращать массу съеденной еды

    // Урок 3. Статические методы и переменные
//    private static HashMap<Cat, LiveStatus> inventory = new HashMap<>();
    private static Set<Cat> inventory = new HashSet<>();
    private static int count = 0;

    // Урок 4. Создать у кошки константы “количество глаз”, “минимальный вес” и “максимальный вес”.
    private static final double MIN_WEIGHT = 1000.0;
    private static final double MAX_WEIGHT = 9000.0;

    // Урок 5. Создание объектов и конструктор
    public Cat(String name, double weight, Eyes eyes) {
        this.liveStatus = LiveStatus.ALIVE;
        this.name = name;
        this.originWeight = weight;
        this.weight = weight;
        this.eyes = eyes;
        this.number = count;
        catBorn();
    }

    public Cat(String name, double weight) {
        this(name, weight, Eyes.TWO);
    }

    public Cat(String name) {
        this(name, 1500.0 + 3000.0 * Math.random(), Eyes.TWO);
    }

    public Cat(String name, Eyes eyes) {
        this(name, 1500.0 + 3000.0 * Math.random(), eyes);
    }

    public Cat() {
        this("Безымянный", 1500.0 + 3000.0 * Math.random(), Eyes.TWO);
    }


    public void meow() {
        if (liveStatus == LiveStatus.DEAD) {
            System.out.println("Мёртвые котики не мяукают");
            return;
        }
        weight = weight - 10;
        System.out.println("Meow");
        checkWeight();
    }

    public void feed(Double amount) {
        if (liveStatus == LiveStatus.DEAD) {
            System.out.println("Мёртвые котики не куашют");
            return;
        }
        weight = weight + amount;
        lastMeal = amount;  // Урок 2. Создать в классе Cat метод, который будет возвращать массу съеденной еды
        checkWeight();
    }

    public void drink(Double amount) {
        if (liveStatus == LiveStatus.DEAD) {
            System.out.println("Мёртвые котики не пьют");
            return;
        }
        weight = weight + amount;
        checkWeight();
    }

    public Double getWeight() {
        return weight;
    }

    public void printWeight() {
        if (liveStatus == LiveStatus.DEAD) {
            return;
        }
        System.out.println(doubleTrim(weight));
    }

    // Урок 2. Создать в классе Cat метод, который будет возвращать массу съеденной еды
    public double getLastMeal() {
        return lastMeal;
    }

    // Урок 2. Создать в классе Cat метод “сходить в туалет”, который будет уменьшать вес кошки и что-нибудь печатать.
    public void defecate() {
        if (liveStatus == LiveStatus.DEAD) {
            System.out.println("Мёртвые котики не какают");
            return;
        }
        weight -= weight / 10 * Math.random();      // Ходит не более чем на 1/10 своей массы. Угадиться до смерти не получится :)
        System.out.println("[Котик опрожнился]");
        meow();
    }

    private void catBorn() {
        count++;
        inventory.add(this);
        System.out.println("\nНаши учёные создали котика! \n" + this.toString() + "\n");
    }


    private void catDied() {
        liveStatus = LiveStatus.DEAD;
        System.out.println("\nНаши учёные потеряли котика ;( \n");
    }

    public LiveStatus getLiveStatus() {
        return liveStatus;
    }

    // Урок 6. Инкапсуляция, геттеры и сеттеры
    public void setEyes(Eyes eyes) {
        this.eyes = eyes;
    }

    public Eyes getEyes() {
        return eyes;
    }

    public String whatHeDoes() {
        if (this.liveStatus.equals(LiveStatus.DEAD)) {
            return "-----";
        }
        if (weight > originWeight) {
            return "Sleeping";
        } else {
            return "Playing";
        }
    }

    public LiveStatus checkWeight() {
        if (weight < MIN_WEIGHT) {
            System.out.println("Котейка отощал и умер");
            catDied();
            return LiveStatus.DEAD;
        } else if (weight > MAX_WEIGHT) {
            System.out.println("Котейка пережрал и умер");
            catDied();
            return LiveStatus.DEAD;
        }
        return LiveStatus.ALIVE;
    }


    @Override
    public String toString() {
        return "Испытуемый № " + number +
                " {Кличка '" + name +
                "', статус: " + liveStatus.status +
                ", занятие: " + whatHeDoes() +
                ", глазки: " + eyes.eyes() +
                ", исходный вес: " + doubleTrim(originWeight) +
                ", текущий вес: " + doubleTrim(weight) +
                '}';
    }


    public String doubleTrim(Double number) {
        return String.format("%.2f грамм", number);
    }

    private static int getNumberAllCats() {
        return inventory.size();
    }


    public static void printStatistic() {
        System.out.println("\n***************************************************************");
        System.out.println("Количество котиков, участвующих в бесчеловечном эксеприменте: "
                + getNumberAllCats());
        if (inventory.size() > 0) {
            int alive = getNumberCats(LiveStatus.ALIVE);
            if (alive > 0) {
                System.out.println("\tЖивы на текущий момент: " + alive);
            }
            int dead = getNumberCats(LiveStatus.DEAD);
            if (dead > 0) {
                System.out.println("\tЗамучены до смерти: " + dead);
            }
        }
        System.out.println("\n***************************************************************");
    }

    public static void printListAllCats() {
        System.out.println("\n************* Список всех участников эксперимента *************");

        for (Cat cat : inventory) {
            System.out.println(cat.toString());
        }
    }

    public static int getNumberCats(LiveStatus countedStatus) {
        int count = 0;
        for (Cat cat : inventory) {
            if (cat.liveStatus == countedStatus) {
                count++;
            }
        }
        return count;
    }

    // Урок  7. Копирование объектов. Создать у кошки метод создания её “глубокой” копии.
    public Cat deepCopy() {
        System.out.println("Подготовлен котейка для клонирования:");
        System.out.println(toString());
        System.out.println("Уникальный хэш генома: " + hashCode());
        System.out.println(".....   Идёт процесс клонирования  .....");
        Cat deepCopyCat = new Cat(this);
        System.out.println("Уникальный хэш генома: " + deepCopyCat.hashCode());
        return deepCopyCat;
    }

    // Приватный конструктор для клонирования
    private Cat(Cat cat) {
        this.liveStatus = cat.liveStatus;
        this.name = cat.name;
        this.number = cat.number;
        this.originWeight = cat.originWeight;
        this.weight = cat.weight;
        this.eyes = cat.eyes;
        this.lastMeal = cat.lastMeal;
        catBorn();
    }


}