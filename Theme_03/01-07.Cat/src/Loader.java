import enums.Eyes;

public class Loader {
    public static void main(String[] args) {
        Cat.printStatistic();

        // Урок 1. Перекормить кошку, чтобы она взорвалась
        System.out.println("\nОПЫТ 1. ФАЗА 1 - перекормить кошку, чтобы она взорвалась");
        Cat cat = new Cat("Первый Кот");
        cat.feed(333d);
        cat.meow();
        cat.drink(222d);
        System.out.println(cat.toString());
        while (cat.getLiveStatus() == LiveStatus.ALIVE) {
            cat.feed(Math.random() * 800 + 200);
            System.out.println(cat.toString());
        }

        Cat.printStatistic();

        // Урок 1. "Замяукать" кошку до смерти
        System.out.println("\nОПЫТ 1. ФАЗА 2 - \"замяукать\" кошку до смерти");
        Cat meowCat = new Cat("Слепая Мявка", 1100d, Eyes.WITHOUT_EYES);
        System.out.println(meowCat.toString());
        do {
            meowCat.meow();
            System.out.println(meowCat.toString());
        } while (meowCat.getLiveStatus() == LiveStatus.ALIVE);

        Cat.printStatistic();


        // Урок 2. Метод, параметры, return
        System.out.println("\nОПЫТ 2. ФАЗА 1 - метод, который будет возвращать массу съеденной еды\n");
        Cat thirdCat = new Cat("Слепой обжора", Eyes.WITHOUT_EYES);
        System.out.println(thirdCat.whatHeDoes());
        thirdCat.printWeight();
        thirdCat.feed(200d);
        thirdCat.printWeight();
        thirdCat.feed(100d);
        thirdCat.printWeight();
        System.out.println("В последний раз кошак слопал " + thirdCat.getLastMeal() + " грамм еды");

        Cat.printStatistic();


        System.out.println("\nОПЫТ 2. ФАЗА 2 - метод “сходить в туалет”, который будет уменьшать вес кошки и что-нибудь печатать.\n");
        System.out.println(thirdCat.toString());
        thirdCat.defecate();
        System.out.println(thirdCat.toString());

        Cat.printStatistic();

        // Урок 6. Инкапсуляция, геттеры и сеттеры
        System.out.println("\nОПЫТ 3. ФАЗА 1 - геттеры.\n");
        Cat anotherCat = new Cat("Кутузов");
        System.out.println("Сколько глаз у вашего Кутузова? \n" + anotherCat.getEyes().eyes());
        System.out.println("А у настоящего 1!");
        System.out.println("\nОПЫТ 3. ФАЗА 2 - сеттеры.\n");
        anotherCat.setEyes(Eyes.ONE);
        System.out.println("Привели в соответствие! Теперь и у кота - " + anotherCat.getEyes().eyes());
        System.out.println(anotherCat.toString());

        Cat.printStatistic();

        // Урок  7. Копирование объектов. Создать у кошки метод создания её “глубокой” копии.
        System.out.println("\nОПЫТ 4. ФАЗА 1 - создание “глубокой” копии.\n");
        Cat cloneCat = cat.deepCopy();
        System.out.println(cloneCat.toString());

        Cat.printStatistic();

        System.out.println("После успешного клоинрования мёртвого котейки переходим к живым");
        System.out.println("\nОПЫТ 4. ФАЗА 2 - создание “глубокой” копии живого котейки.\n");
        Cat cloneCat2 = anotherCat.deepCopy();
        System.out.println(cloneCat2.toString());

        Cat.printStatistic();

        Cat.printListAllCats();
    }
}