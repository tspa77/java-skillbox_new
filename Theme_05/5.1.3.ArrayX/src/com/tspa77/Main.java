package com.tspa77;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Введите длину стороны квадрата");
        Scanner scanner = new Scanner(System.in);

        int size = scanner.nextInt();
        String[] array = new String[size];
        int begin = 0;
        int end = size - 1;

        while (begin <= end) {
            // Создаём временный массив, для строки
            char[] stringArray = new char[size];
            for (int j = 0; j < size; j++) {
                stringArray[j] = ' ';
            }
            // расставляем Х в строке-массиве
            stringArray[begin] = 'Х';
            stringArray[end] = 'Х';
            // расставляем строки в основном массиве (строки зеркальны относительно центра)
            array[begin] = String.valueOf(stringArray);
            array[end] = String.valueOf(stringArray);
            // двигаем указатели "внутрь"
            begin++;
            end--;
        }

        for (String s : array) {
            System.out.println(s);
        }
    }
}
