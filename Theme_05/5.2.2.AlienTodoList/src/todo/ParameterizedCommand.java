package todo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ParameterizedCommand implements Command {
    private Pattern parametersPattern;

    protected abstract Pattern compileParametersPattern();

    protected String[] getParameters(Matcher matcher) {
        String parameters[] = new String[matcher.groupCount()];

        for(int i = 0; i < parameters.length; i++)
            parameters[i] = matcher.group(i + 1);

        return parameters;
    }

    @Override
    public void execute(String parameters) {
        if(parameters == null) {
            printIllegalFormatErrorMessage();
            return;
        }

        if(parametersPattern == null)
            parametersPattern = compileParametersPattern();

        Matcher matcher = parametersPattern.matcher(parameters);

        if(!matcher.find()) {
            printIllegalFormatErrorMessage();
            return;
        }

        execute(getParameters(matcher));
    }

    protected abstract void execute(String parameters[]);

    protected void printIllegalFormatErrorMessage() {
        System.out.println("Некорректно введена команда");
    }
}
