package todo.command;

import todo.Command;
import java.util.List;

public class ListCommand implements Command {
    private final List<String> todos;

    public ListCommand(List<String> todos) {
        this.todos = todos;
    }

    @Override
    public void execute(String parameters) {
        System.out.println("=== СПИСОК ДЕЛ ======================");

        if(todos.isEmpty())
            System.out.println("На данный момент список пуст :(");
        else
            for(int i = 0; i < todos.size(); i++)
                System.out.printf("%3d: %s%n", i, todos.get(i));

        System.out.println("=====================================");
    }
}
