package todo.command;

import todo.ParameterizedCommand;
import java.util.List;
import java.util.regex.Pattern;

public class EditCommand extends ParameterizedCommand {
    private final List<String> todos;

    public EditCommand(List<String> todos) {
        this.todos = todos;
    }

    @Override
    protected Pattern compileParametersPattern() {
        return Pattern.compile("^(0|[1-9]\\d*)\\s+(.+)$");
    }

    @Override
    protected void execute(String parameters[]) {
        String inputIndex = parameters[0];
        String inputTitle = parameters[1];

        int index = Integer.parseInt(inputIndex);

        try {
            todos.set(index, inputTitle);
        }
        catch(IndexOutOfBoundsException exception) {
            if(todos.isEmpty())
                System.out.println("Выход за пределы списка, список пуст");
            else
                System.out.printf("Выход за пределы списка, индекс должен быть в диапазоне [0, %d)%n", todos.size());

            return;
        }

        System.out.printf("Задача под индексом %d успешно заменена на \"%s\"!%n", index, inputTitle);
    }

    @Override
    protected void printIllegalFormatErrorMessage() {
        System.out.println("Некорректно введена команда, допустимый формат: ");
        System.out.println("EDIT <index> <title>");
    }
}
