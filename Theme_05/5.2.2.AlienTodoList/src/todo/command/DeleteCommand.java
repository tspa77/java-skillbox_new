package todo.command;

import todo.ParameterizedCommand;
import java.util.List;
import java.util.regex.Pattern;

public class DeleteCommand extends ParameterizedCommand {
    private final List<String> todos;

    public DeleteCommand(List<String> todos) {
        this.todos = todos;
    }

    @Override
    protected Pattern compileParametersPattern() {
        return Pattern.compile("^(0|[1-9]\\d*)$");
    }

    @Override
    protected void execute(String parameters[]) {
        int index = Integer.parseInt(parameters[0]);

        try {
            todos.remove(index);
        }
        catch(IndexOutOfBoundsException exception) {
            if(todos.isEmpty())
                System.out.println("Выход за пределы списка, список пуст");
            else
                System.out.printf("Выход за пределы списка, индекс должен быть в диапазоне [0, %d)%n", todos.size());

            return;
        }

        System.out.printf("Задача под индексом %d успешно удалена!%n", index);
    }

    @Override
    protected void printIllegalFormatErrorMessage() {
        System.out.println("Некорректно введена команда, допустимый формат: ");
        System.out.println("DELETE <index>");
    }
}
