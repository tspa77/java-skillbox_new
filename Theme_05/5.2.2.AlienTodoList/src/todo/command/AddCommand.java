package todo.command;

import todo.ParameterizedCommand;
import java.util.List;
import java.util.regex.Pattern;

public class AddCommand extends ParameterizedCommand {
    private final List<String> todos;

    public AddCommand(List<String> todos) {
        this.todos = todos;
    }

    @Override
    protected Pattern compileParametersPattern() {
        return Pattern.compile("^(?:(0|[1-9]\\d*)\\s+)?(.+)$");
    }

    @Override
    protected void execute(String parameters[]) {
        String inputIndex = parameters[0];
        String inputTitle = parameters[1];

        try {
            todos.add(inputIndex != null? Integer.parseInt(inputIndex): todos.size(), inputTitle);
        }
        catch(IndexOutOfBoundsException exception) {
            System.out.printf("Выход за пределы списка, индекс должен быть в диапазоне [0, %d]%n", todos.size());
            return;
        }

        System.out.printf("Задача \"%s\" успешно добавлена!%n", inputTitle);
    }

    @Override
    protected void printIllegalFormatErrorMessage() {
        System.out.println("Некорректно введена команда, допустимый формат: ");
        System.out.println("ADD [<index>] <title>");
    }
}
