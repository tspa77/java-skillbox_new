package todo.command;

import todo.Command;
import todo.TodoList;

public class ExitCommand implements Command {
    private final TodoList list;

    public ExitCommand(TodoList list) {
        this.list = list;
    }

    @Override
    public void execute(String parameters) {
        list.interrupt();
    }
}
