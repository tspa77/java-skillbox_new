package todo;

import todo.command.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TodoList {
    private static final String LIST_COMMAND = "LIST";
    private static final String ADD_COMMAND = "ADD";
    private static final String EDIT_COMMAND = "EDIT";
    private static final String DELETE_COMMAND = "DELETE";
    private static final String EXIT_COMMAND = "EXIT";

    private List<String> todos;

    private CommandResolver resolver;

    private boolean interrupted;

    private List<String> getTodos() {
        if(todos == null)
            todos = new ArrayList<>();

        return todos;
    }

    private CommandResolver getResolver() {
        if(resolver == null)
            resolver = new CachedCommandResolver(this::resolveCommand);

        return resolver;
    }

    private Command getCommand(String name) {
        return getResolver().resolve(name);
    }

    private Command resolveCommand(String name) {
        switch(name) {
            case LIST_COMMAND:
                return new ListCommand(getTodos());

            case ADD_COMMAND:
                return new AddCommand(getTodos());

            case EDIT_COMMAND:
                return new EditCommand(getTodos());

            case DELETE_COMMAND:
                return new DeleteCommand(getTodos());

            case EXIT_COMMAND:
                return new ExitCommand(this);
        }

        throw new IllegalArgumentException(String.format("Команда \"%s\" не существует", name));
    }

    private static Pattern compileCommandPattern(String... commandNames) {
        return Pattern.compile("^(" + String.join("|", commandNames) + ")(?:\\s+(.+))?$");
    }

    public void interrupt() {
        interrupted = true;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);

        Pattern commandPattern = compileCommandPattern(
            LIST_COMMAND, ADD_COMMAND, EDIT_COMMAND, DELETE_COMMAND, EXIT_COMMAND
        );

        while(!interrupted) {
            System.out.print("?: ");
            String input = scanner.nextLine();
            Matcher matcher = commandPattern.matcher(input.trim());

            if(matcher.find()) {
                String commandName = matcher.group(1);
                String commandParameters = matcher.group(2);
                Command command = getCommand(commandName);
                command.execute(commandParameters);
            }
            else
                System.out.println("Не удалось разобрать команду, попробуйте еще раз...");
        }
    }

    public static void main(String... args) {
        TodoList todoList = new TodoList();
        todoList.run();
    }
}
