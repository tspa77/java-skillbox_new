package todo;

public interface Command {
    public abstract void execute(String parameters);
}
