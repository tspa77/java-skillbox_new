package todo;

public interface CommandResolver {
    public abstract Command resolve(String name);
}
