package todo;

import java.util.HashMap;
import java.util.Map;

public class CachedCommandResolver implements CommandResolver {
    private final CommandResolver origin;

    private Map<String, Command> cache;

    public CachedCommandResolver(CommandResolver origin) {
        this.origin = origin;
    }

    @Override
    public Command resolve(String name) {
        Command command;

        if(cache != null)
            command = cache.get(name);
        else {
            cache = new HashMap<>();
            command = null;
        }

        if(command == null) {
            command = origin.resolve(name);
            cache.put(name, command);
        }

        return command;
    }
}
