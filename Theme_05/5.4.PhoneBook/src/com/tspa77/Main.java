package com.tspa77;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    private static final String RUSSIAN_NAME = "[А-ЯЁ&&[^ЪЬЫ]]{1}[а-яё]+";
    private static final String COMMAND_LIST = "LIST";
    private static final String COMMAND_NAME = "NAME";
    private static final String COMMAND_NUMBER = "NUMBER";

    private static Map<Long, String> phoneBook = new HashMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // Раскомментировать для отладки
        phoneBook.put(987654321L, "Чан Джеки");
        phoneBook.put(987654322L, "Аарон Пол ");
        phoneBook.put(123456789L, "Бэйкон Кевин");
        phoneBook.put(654321L, "Ли Брюс");
        phoneBook.put(654322L, "Ли Брюс");
        phoneBook.put(555111555L, "Тихонов Вячеслав Васильевич");

        while (true) {
            System.out.println("\nВведите поисковый запрос: ");
            String inputString = reader.readLine().trim();
            String command = recognizeInputCommand(inputString);

            switch (command) {
                case COMMAND_LIST:
                    printSortedPhoneBook();
                    break;
                case COMMAND_NAME:
                    handlerName(inputString);
                    break;
                case COMMAND_NUMBER:
                    handlerNumber(inputString);
                    break;
                default:
                    System.out.println("Ошибка ввода.\nУбедитесь, что в номере телефона" +
                            " содержится не менее 6 цифр, а ФИО начинаются с заглавных" +
                            " букв и разделены пробелом.");
            }
        }
    }


    private static String recognizeInputCommand(String inputString) {
        String command = "";

        if (inputString.equals("LIST")) {
            command = COMMAND_LIST;
            return command;
        }
        if (isNumber(inputString)) {
            command = COMMAND_NUMBER;
            return command;
        }
        if (isName(inputString)) {
            command = COMMAND_NAME;
            return command;
        }
        return command;
    }

    private static void printSortedPhoneBook() {
        phoneBook.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(entry -> System.out.printf("%s \t %d \n", entry.getValue(), entry.getKey()));
    }


    private static void handlerNumber(String inputString) {
        Long number = toNumeric(inputString);
        if (phoneBook.containsKey(number)) {
            System.out.println(phoneBook.get(number));
        } else {
            writeToBaseByNumber(number);
            System.out.println("Запись добавлена");
        }
    }


    private static void handlerName(String inputString) {
        if (phoneBook.containsValue(inputString)) {
            printNumbersOfUser(inputString);
        } else {
            writeToBaseByName(inputString);
            System.out.println("Запись добавлена");
        }
    }


    private static boolean isNumber(String number) {
        number = number.replaceAll("\\D", "");
        return number.length() > 5 && number.length() < 18;
    }

    private static long toNumeric(String number) {
        return Long.parseLong(number.replaceAll("\\D", ""));
    }

    private static boolean isName(String name) {
        //У граждан Казахстана, например, может не оказаться отчества.
        String fullNameTemplate = RUSSIAN_NAME + "\\s+" + RUSSIAN_NAME + "(\\s+" + RUSSIAN_NAME + ")?";
        return name.matches(fullNameTemplate);
    }

    private static void printNumbersOfUser(String inputString) {
        for (Map.Entry<Long, String> entry : phoneBook.entrySet()) {
            if (entry.getValue().equals(inputString)) {
                System.out.println(entry.getKey());
            }
        }
    }


    private static void writeToBaseByNumber(Long number) {
        // Записываем в базу по новому номеру
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Такого телефона нет в базе. Заносим.");

        while (true) {
            System.out.println("Введите имя: ");
            String inputString = "";
            try {
                inputString = reader.readLine().trim();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!isName(inputString)) {
                System.out.println("Ошибка ввода.\nУбедитесь, что ФИО начинаются" +
                        " с заглавных букв и разделены пробелом.");
            } else {
                phoneBook.put(number, inputString);
                return;
            }
        }
    }

    private static void writeToBaseByName(String inputString) {
        // Записываем в базу по новому имени
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Такого имени нет в базе. Заносим.");
        while (true) {
            System.out.println("Введите номер: ");
            String strNumber = "";
            try {
                strNumber = reader.readLine().trim();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!isNumber(strNumber)) {
                System.out.println("Ошибка ввода.\nУбедитесь, что в номере телефона" +
                        " содержится не менее 6 цифр");
            } else {
                Long number = toNumeric(strNumber);
                if (phoneBook.containsKey(number)) {
                    System.out.println("Такой номер уже есть в базе.");
                } else {
                    phoneBook.put(number, inputString);
                    return;
                }
            }
        }
    }

}