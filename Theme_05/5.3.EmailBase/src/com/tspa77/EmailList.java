package com.tspa77;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class EmailList {
    private static final String ADD = "ADD";
    private static final String LIST = "LIST";

    private Set<String> setEmails;


    private EmailList() {
        setEmails = new HashSet<>();
    }


    private void list() {
        if (setEmails.size() == 0) {
            System.out.println("Список пуст");
            return;
        }
        for (String email : setEmails) {
            System.out.println(email);
        }

    }


    private boolean checkEmail(String email) {
        return email.toLowerCase().matches("^[a-z0-9._%+-]+@[a-z0-9-]+.+.[a-z]{2,4}$");
    }


    private void add(String email) {
        setEmails.add(email);
        System.out.printf("Email %s добавлен в список%n", email);
    }


    private void run() {
        Scanner scanner = new Scanner(System.in);
        String inputLine;
        String email;
        String[] command;


        while (true) {
            System.out.println("Введите команду: ");
            inputLine = scanner.nextLine().trim();

            if (inputLine.equals(LIST)) {
                list();
                continue;
            }

            command = inputLine.split("\\s+", 2);

            if (command[0].equals(ADD) && command.length == 2) {
                email = command[1].trim();
                if (checkEmail(email)) {
                    add(email);
                    continue;
                } else {
                    System.out.println("Некорректный email");
                    continue;
                }
            }
            System.out.println("Неизвестная комманда");
        }
    }

    public static void main(String[] args) {
        EmailList emailList = new EmailList();
        emailList.run();
    }
}
