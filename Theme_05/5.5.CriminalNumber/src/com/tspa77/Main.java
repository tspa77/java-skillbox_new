package com.tspa77;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {

        char[] allowedSymbols = {'А', 'В', 'Е', 'К', 'М', 'Н', 'О', 'Р', 'С', 'Т', 'У', 'Х'};

        String[] regions = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "113", "14", "15", "16", "116", "716", "17", "18", "19", "21", "121", "22", "23", "93", "123", "24", "84", "88", "102", "124", "25", "125", "26", "126", "27", "28", "29", "30", "31", "32", "33", "34", "134", "35", "36", "136", "37", "38", "85", "138", "39", "91", "40", "41", "42", "142", "43", "44", "45", "46", "47", "147", "48", "49", "50", "90", "150", "190", "750", "51", "52", "152", "53", "54", "154", "55", "56", "57", "58", "59", "81", "159", "60", "61", "161", "62", "63", "163", "763", "64", "164", "65", "66", "96", "196", "67", "68", "69", "70", "71", "72", "73", "173", "74", "174", "75", "80", "76", "77", "97", "99", "177", "197", "199", "777", "799", "78", "98", "178", "198", "79", "82", "83", "86", "186", "87", "89", "92", "94", "95"};
        Arrays.sort(regions);

        System.out.println("Формируем базу блатных номеров. Ожидайте...");
        System.out.print("База формируется в ArrayList.");

        long startTime = System.currentTimeMillis();
        ArrayList<String> criminalDigitCombo = new ArrayList<>();

        for (int i = 1; i < 10; i++) {
            criminalDigitCombo.add("00" + i);
            criminalDigitCombo.add("0" + i + "0");
            criminalDigitCombo.add(i + "00");
            criminalDigitCombo.add("" + (i * 111));
        }

        ArrayList<String> criminalNumbers = new ArrayList<>();
        for (String region : regions) {
            for (char sim1 : allowedSymbols) {
                for (String digit : criminalDigitCombo) {
                    for (char sim2 : allowedSymbols) {
                        for (char sim3 : allowedSymbols) {
                            criminalNumbers.add(sim1 + digit + sim2 + sim3 + region);
                        }
                    }
                }
            }
        }

        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("\t Всего комбинаций: " + criminalNumbers.size()
                + ". Затрачено времени: " + (double) estimatedTime / 1000 + " секунд");


        // Клонируем в HashSet
        System.out.print("База клонируется в HashSet.");
        startTime = System.currentTimeMillis();
        HashSet<String> criminalNumberHS = new HashSet<>(criminalNumbers);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("\t\t Всего комбинаций: " + criminalNumberHS.size()
                + ". Затрачено времени: " + (double) estimatedTime / 1000 + " секунд");

        // Клонируем в TreeSet
        System.out.print("База клонируется в TreeSet.");
        startTime = System.currentTimeMillis();
        TreeSet<String> criminalNumberTS = new TreeSet<>(criminalNumbers);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("\t\t Всего комбинаций: " + criminalNumberTS.size()
                + ". Затрачено времени: " + (double) estimatedTime / 1000 + " секунд\n");


        // Начинает работать
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("\nВведите номер: ");
            String number = reader.readLine().replace(" ", "").toUpperCase();


            // Обычный поиск
            boolean found = false;
            startTime = System.nanoTime();
            for (String criminalNumber : criminalNumbers) {
                if (criminalNumber.equals(number)) {
                    found = true;
                    break;
                }
            }
            estimatedTime = System.nanoTime() - startTime;
            System.out.println("Затрачено времени на обычный поиск: \t" + (double) estimatedTime / 1000 + " микросекунд");
            printResult(found);

            // Бинарный поиск
            Collections.sort(criminalNumbers);
            startTime = System.nanoTime();
            int result = Collections.binarySearch(criminalNumbers, number);
            found = result >= 0;
            estimatedTime = System.nanoTime() - startTime;
            System.out.println("Затрачено времени на бинарный поиск: \t" + (double) estimatedTime / 1000 + " микросекунд");
            printResult(found);

            // HashSet поиск
            startTime = System.nanoTime();
            found = criminalNumberHS.contains(number);
            estimatedTime = System.nanoTime() - startTime;
            System.out.println("Затрачено времени на HashSet поиск: \t" + (double) estimatedTime / 1000 + " микросекунд");
            printResult(found);

            // TreeSet поиск
            startTime = System.nanoTime();
            found = criminalNumberTS.contains(number);
            estimatedTime = System.nanoTime() - startTime;
            System.out.println("Затрачено времени на TreeSet поиск: \t" + (double) estimatedTime / 1000 + " микросекунд");
            printResult(found);
        }
    }

    public static void printResult(boolean found) {
        if (found) {
            System.out.println("Блатной");
        } else {
            System.out.println("Не блатной");
        }
    }


}
