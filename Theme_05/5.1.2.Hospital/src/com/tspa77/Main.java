package com.tspa77;

import java.util.Arrays;
import java.util.Random;

public class Main {
    private static final double MIN_TEMPERATURE = 32;
    private static final double MAX_TEMPERATURE = 40;
    private static final double NORMAL_MIN_TEMPERATURE = 36.2;
    private static final double NORMAL_MAX_TEMPERATURE = 36.9;


    public static void main(String[] args) {

        // 1. Classic handmade
        double[] temperature = generateArray(30);
        double averageTemperature = getAverageTemperature(temperature);
        int numberHealthy = getNumberHealthy(temperature);
        printReport(averageTemperature, numberHealthy);


        // 2. New cool Stream
//        double[] temperatureStream = temperature; // проверил на одном входном массиве - всё Ок, в показаниях не путаются
        double[] temperatureStream = generateArrayStream(30);
        double averageTemperatureStream = getAverageTemperatureStream(temperatureStream);
        int numberHealthyStream = getNumberHealthyStream(temperatureStream);
        printReport(averageTemperatureStream, numberHealthyStream);

    }


    public static void printReport(double averageTemperature, int numberHealthy) {
        System.out.printf("\nСредняя температура по больнице %.1f градуса." +
                "\nКоличество здоровых пациентов %d", averageTemperature, numberHealthy);
    }


    // 1. Classic handmade
    private static double[] generateArray(int i) {
        double[] array = new double[i];
        for (int j = 0; j < i; j++) {
            array[j] = (Math.random() * (MAX_TEMPERATURE - MIN_TEMPERATURE) + MIN_TEMPERATURE);
        }
        return array;
    }

    private static double getAverageTemperature(double[] temperature) {
        double summ = 0f;
        for (double v : temperature) {
            summ += v;
        }
        return summ / temperature.length;

    }

    private static int getNumberHealthy(double[] temperature) {
        int count = 0;
        for (double v : temperature) {
            if (v > NORMAL_MIN_TEMPERATURE && v < NORMAL_MAX_TEMPERATURE) {
                count++;
            }
        }
        return count;
    }


    // 2. New cool Stream
    private static double[] generateArrayStream(int i) {
        return new Random().doubles(i, MIN_TEMPERATURE, MAX_TEMPERATURE).toArray();
    }

    private static double getAverageTemperatureStream(double[] temperatureStream) {
        return Arrays.stream(temperatureStream)
                .summaryStatistics()
                .getAverage();
    }

    private static int getNumberHealthyStream(double[] temperatureStream) {
        return (int) Arrays.stream(temperatureStream)
                .filter(num -> num > NORMAL_MIN_TEMPERATURE && num < NORMAL_MAX_TEMPERATURE)
                .count();
    }


}
