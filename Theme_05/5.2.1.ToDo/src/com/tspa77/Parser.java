package com.tspa77;

public class Parser {

    String command;
    String task;
    int number;

    public Parser(String inputLine) {
        parseLine(inputLine);
    }


    private void parseLine(String text) {
        String[] commandsArray = text.split("\\s");
        int length = commandsArray.length;
        number = -1;
        command = commandsArray[0].toUpperCase();

        if (length > 1) {
            if (commandsArray[1].matches("^\\d+$")) {
                number = Integer.parseInt(commandsArray[1]);
                if (length > 2) {
                    task = text.substring(commandsArray[0].length()
                            + commandsArray[1].length() + 1).trim();
                }
            } else {
                task = text.substring(command.length()).trim();
            }
        }
    }


    public String getCommand() {
        return command;
    }

    public String getTask() {
        return task;
    }

    public Integer getNumber() {
        return number;
    }

    // debug
    @Override
    public String toString() {
        return "Parser{" +
                "command='" + command + '\'' +
                ", task='" + task + '\'' +
                ", number=" + number +
                '}';
    }
}
