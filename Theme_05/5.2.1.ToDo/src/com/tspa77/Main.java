package com.tspa77;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {


    public static void main(String[] args) throws IOException {

        SimpleToDo toDo = new SimpleToDo();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputLine;
        String command;
        String task;
        int number;


        while (true) {
            System.out.println("Ожидаю команду: ");
            inputLine = reader.readLine();
            if (inputLine.isBlank()) {
                System.out.println("Введите хоть что-то :(");
                continue;
            }
            Parser parser = new Parser(inputLine);
            task = parser.getTask();
            number = parser.getNumber();
            command = parser.getCommand();

            switch (command) {
                case "LIST":
                    toDo.list();
                    break;
                case "ADD":
                    toDo.add(number, task);
                    break;
                case "EDIT":
                    toDo.edit(number, task);
                    break;
                case "DELETE":
                    toDo.delete(number);
                    break;
                default:
                    System.out.println("Неопознанная команда. Поддерживаемые команды:\n" +
                            "LIST - выводит на экран список дел\n" +
                            "ADD [task] - добавить задачу\n" +
                            "ADD [number] [task] - добавить задачу на указанную позицию\n" +
                            "EDIT [number] [task] - изменить задачу с указанным номером\n" +
                            "DELETE [number] - удалить задачу с указанным номером\n"
                    );
                    break;
            }
        }


    }
}
