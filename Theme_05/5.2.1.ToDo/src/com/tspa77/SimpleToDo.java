package com.tspa77;

import java.util.ArrayList;

public class SimpleToDo {
    ArrayList<String> toDo;

    public SimpleToDo() {
        toDo = new ArrayList<>();
        // todo Тест! Закоментить перед продакшеном!
        toDo.add("Подъём");
        toDo.add("Умывание");
        toDo.add("Завтрак");
    }

    public void list() {
        for (int i = 0; i < toDo.size(); i++) {
            System.out.println(i + 1 + ". " + toDo.get(i));
        }
        if (toDo.size() == 0) {
            System.out.println("На сегодня дел нет. Может по пивку? ;)");
        }
    }

    public void add(String task) {
        toDo.add(task);
        System.out.println("Задача добавлена");
    }

    public void add(int number, String task) {
        if (number == -1) {
            add(task);
            return;
        }
        if (number > 0 && number - 1 <= toDo.size()) {
            toDo.add(number - 1, task);
            System.out.println("Задача " + task + " добавлена");
        } else {
            System.out.println("Недопустимый аргумент");
        }
    }

    public void edit(int number, String task) {
        if (number > 0 && number - 1 < toDo.size()) {
            toDo.set(number - 1, task);
            System.out.println("Задача " + task + " обновлена");
        } else {
            System.out.println("Недопустимый аргумент");
        }
    }

    public void delete(int number) {
        if (number > 0 && number - 1 < toDo.size()) {
            String task = toDo.remove(number - 1);
            System.out.println("Задача '" + task + "' удалена");
        } else {
            System.out.println("Недопустимый аргумент");
        }

    }
}
