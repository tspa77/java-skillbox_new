package com.tspa77;

import java.util.Arrays;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        // write your code here

        String text = "Каждый охотник желает знать, где сидят фазаны";
        String[] colors = text.split(",?\\s+");


        // Вариант 1
        String reverseColors[] = new String[colors.length];

        for (int i = colors.length - 1; i >= 0; i--) {
            reverseColors[colors.length - 1 - i] = colors[i];
        }

        System.out.println();
        for (String color : reverseColors) {
            System.out.println(color);
        }

        System.out.println("\n\n");

        // Вариант 2

        Collections.reverse(Arrays.asList(colors));
        System.out.println(Arrays.asList(colors));
    }
}
