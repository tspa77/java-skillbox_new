package org.sample;

import com.skillbox.airport.Flight;
import com.skillbox.airport.Terminal;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CustomOnlineScoreboard {
    private List<Terminal> terminals;

    public enum Mode {
        SingleThread,
        MultiThreaded
    }

    public CustomOnlineScoreboard(List<Terminal> terminals) {
        this.terminals = terminals;
        System.out.println("Интерактивное онлайн-табло \"Нью-Васюки Аэрлайнс\"" +
                "приветствует Вас!");
    }

    public void showInformation(Mode mode, Consumer<String> consumer) {
        Stream<String> stream = getFlightsStream(Flight.Type.DEPARTURE, Duration.ofHours(4));

        if (mode == Mode.MultiThreaded) {
            stream = stream.parallel();
        }

        stream.forEachOrdered(consumer);
    }

    private Stream<String> getFlightsStream(Flight.Type type, Duration duration) {
        return terminals
                .stream()
                .flatMap(terminal -> terminal.getFlights()
                        .stream()
                        .filter(flight -> flight.getType().equals(type))
                        .filter(flight -> isDateInInterval(flight.getDate(), duration))
                        .map(flight -> new TerminalFlight(terminal, flight)))
                .sorted(Comparator.comparing(tf -> tf.flight.getDate()))
                .map(tf -> tf.flight + " / TERMINAL " + tf.terminal.getName());
    }

    private boolean isDateInInterval(Date date, Duration duration) {
        LocalDateTime flyingDataTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime nowDataTime = LocalDateTime.now();
        return flyingDataTime.isAfter(nowDataTime) && flyingDataTime.isBefore(nowDataTime.plus(duration));
    }

    private static class TerminalFlight {
        final Terminal terminal;
        final Flight flight;

        TerminalFlight(Terminal terminal, Flight flight) {
            this.flight = flight;
            this.terminal = terminal;
        }
    }

}
