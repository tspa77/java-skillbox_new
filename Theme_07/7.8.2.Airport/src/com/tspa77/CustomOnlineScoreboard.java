package com.tspa77;

import com.skillbox.airport.Flight;
import com.skillbox.airport.Terminal;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class CustomOnlineScoreboard {
    private Duration intervalInHours;
    private Flight.Type typeFlight;
    private List<Terminal> terminals;


    public CustomOnlineScoreboard(List<Terminal> terminals) {
        this.terminals = terminals;
        System.out.println("Интерактивное онлайн-табло" +
                " \"Нью-Васюки Аэрлайнс\" приветствует Вас!");
    }


    private static class TerminalFlight {
        final Terminal terminal;
        final Flight flight;

        TerminalFlight(Terminal terminal, Flight flight) {
            this.flight = flight;
            this.terminal = terminal;
        }
    }


    public void requestedInformation() {
        intervalInHours = requestedTimeInterval();
        typeFlight = requestedFlightType();
        showInformation();
        System.out.println("\n");
//        printInfo();
//        printInfoParallel();
    }

    private Flight.Type requestedFlightType() {
        //todo запросить тип полета - прибытие или отбытие
        return Flight.Type.DEPARTURE;
    }

    private Duration requestedTimeInterval() {
        //todo запросить интересующий интервал в часах
        return Duration.ofHours(4);
    }


    public void showInformation() {
        Stream<String> stream = getFlightsStream(typeFlight, intervalInHours);
        stream.forEachOrdered(System.out::println);
        System.out.println("\n");
    }


    private Stream<String> getFlightsStream(Flight.Type type,
                                            Duration duration) {
        return terminals
                .stream()
                .flatMap(terminal -> terminal.getFlights()
                        .stream()
                        .filter(flight -> flight.getType().equals(type))
                        .filter(flight -> isIncludedInTimeInterval(
                                flight.getDate(), duration))
                        .map(flight -> new TerminalFlight(terminal, flight))
                )
                .sorted(Comparator.comparing(tf -> tf.flight.getDate()))
                .map(tf -> tf.flight + " / TERMINAL " + tf.terminal.getName());
    }


    private boolean isIncludedInTimeInterval(Date date, Duration duration) {
        LocalDateTime flyingDataTime = date.toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime nowDataTime = LocalDateTime.now();
        return flyingDataTime.isAfter(nowDataTime)
                && flyingDataTime
                .isBefore(nowDataTime.plusHours(duration.toHours()));
    }

//    private void printInfo() {
//        terminals
//                .stream()
//                .map(this::getRequestedFlightsFromTerminal)
//                .flatMap(m -> m.entrySet().stream())
//                .sorted(Comparator.comparing(e -> e.getKey().getDate()))
//                .map(entry -> entry.getKey().toString() + " / TERMINAL " + entry.getValue().getName())
//                .forEachOrdered(System.out::println);
//    }
//
//
//    private void printInfoParallel() {
//        Map<Flight, Terminal> flightTerminalMap = new HashMap<>(terminals
//                .parallelStream()
//                .map(this::getRequestedFlightsFromTerminal)
//                .flatMap(m -> m.entrySet().stream())
//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
//        );
//
//        flightTerminalMap.entrySet()
//                .stream()
//                .sorted(Comparator.comparing(e -> e.getKey().getDate()))
//                .map(entry -> entry.getKey().toString() + " / TERMINAL " + entry.getValue().getName())
//                .forEachOrdered(System.out::println);
//    }
//
//
//    private Map<Flight, Terminal> getRequestedFlightsFromTerminal(Terminal terminal) {
//        return terminal.getFlights()
//                .parallelStream()
//                .filter(flight -> flight.getType() == typeFlight)
//                .filter(flight -> isIncludedInTimeInterval(flight.getDate(), intervalInHours))
//                // по поводу фильтра после фильтра: поискал, что лучше - сложный запрос или 2 фильтра.
//                // Вроде пишут что особой разницы нет, поэтому выбрал, тот вариант, что легче читается
//                .collect(Collectors.toMap(f -> f, f -> terminal));  // первый f -> f можно заменить на identity(),
//        // но так однооборазней и понятней ИМХО
//    }


}
