package com.tspa77;

import com.skillbox.airport.Airport;
import com.skillbox.airport.Terminal;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Airport airport = Airport.getInstance();
        List<Terminal> terminals = airport.getTerminals();
        CustomOnlineScoreboard onlineScoreboard = new CustomOnlineScoreboard(terminals);
        onlineScoreboard.requestedInformation();
    }
}
