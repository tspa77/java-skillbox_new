import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;


/**
 * В проекте с сотрудниками с помощью Stream API рассчитать максимальную зарплату тех, кто пришёл в 2017 году.
 */

public class Main {
    private static String staffFile = "data/staff.txt";
    private static String dateFormat = "dd.MM.yyyy";


    public static void main(String[] args) {
        ArrayList<Employee> staff = loadStaffFromFile();

        // Не совсем понял формулировку задания, поэтому если нужен просто поиск/печать суммы максимальной ЗП
        // в году то это будет так:

        System.out.println(getMaxSalaryInYear(staff, 2017));

        // А если задача вывести список людей с максимальной ЗП в году (а их может быть несколько), то я не вижу
        // как в один стрим уместить, т.к. "макс" является терминальным. Всё что смог это придумать это
        // переиспользование первого стрима и его результат поставить фильтром для второго


        printEmployeeWithMaxSalaryInYear(staff, 2017);

    }

//    public static int getMaxSalaryInYear(ArrayList<Employee> staff, int yearOfEmployment) {
//        // т.к. Date.getYear() @Deprecated
//        Calendar calendar = Calendar.getInstance();
//
//        return staff.stream()
//                .filter(e -> {
//                    // я не смог придумать как вместе с преобразование даты в календарь
//                    // всё красиво упаковать в одну строку :(
//                    calendar.setTime(e.getWorkStart());
//                    return calendar.get(Calendar.YEAR) == yearOfEmployment;
//                })
//                .map(Employee::getSalary)
//                .max(Integer::compare)
//                .orElse(0);
//    }


    // Пример преподавателя. То же, но "некрасивый фрагмент" с взятием даты вынесен в отдельный метод и
    // без календаря, через LocalDate, красивее
    // Исправил тип входного аргумента staff на Collection<>, т.к. она итерируется и тем самым расширием возможности
    public static int getMaxSalaryInYear(Collection<Employee> staff, int yearOfEmployment) {
        return staff.stream()
                .filter(e -> getYear(e.getWorkStart()) == yearOfEmployment)
                .map(Employee::getSalary)
                .max(Integer::compare)
                .orElse(0);
    }

    private static int getYear(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).getYear();
    }



    public static void printEmployeeWithMaxSalaryInYear(Collection<Employee> staff, int yearOfEmployment) {
        int maxSalaryInYear = getMaxSalaryInYear(staff, yearOfEmployment);

        Calendar calendar = Calendar.getInstance();
        if (maxSalaryInYear > 0) {
            staff.stream()
                    .filter(e -> {
                        calendar.setTime(e.getWorkStart());
                        return calendar.get(Calendar.YEAR) == yearOfEmployment && e.getSalary() == maxSalaryInYear;
                    })
                    .forEach(System.out::println);
        } else {
            System.out.println("empty");
        }
    }


    private static ArrayList<Employee> loadStaffFromFile() {
        ArrayList<Employee> staff = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(staffFile));
            for (String line : lines) {
                String[] fragments = line.split("\t");
                if (fragments.length != 3) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }
                staff.add(new Employee(
                        fragments[0],
                        Integer.parseInt(fragments[1]),
                        (new SimpleDateFormat(dateFormat)).parse(fragments[2])
                ));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return staff;
    }
}