package com.tspa77

val charPool: List<Char> = ('A'..'Z') + ('0'..'9')
const val MIN_AMOUNT = 10_000L
const val MAX_AMOUNT = 100_000L

// Случайный номер счёта
fun getRandomAccountName() = (1..8)
    .map { (charPool.indices).random() }
    .map(charPool::get)
    .joinToString("");

// Случайная сумма
fun getRandomAmount() = (MIN_AMOUNT..MAX_AMOUNT).random()