package com.tspa77

import java.lang.Thread.sleep
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


fun main() {
    // Создаём банк
    val bank = Bank()
    val transactionCount = 5000 // Количество тестовых транзакций

    // Наполняем случайными счетами
    repeat(10) {
        bank.addAccount(Account(getRandomAmount(), getRandomAccountName()))
    }

    println("Печатаем состояние всех счетов")
    bank.printAccounts()

    val sumAllBankDeposits = bank.getSumAllBankDeposits()
    println("Сумма всех вкладов в банке: $sumAllBankDeposits \n")

    // Имитация highly concurrent
    // Получаем список всех счетов
    val accNumber = bank.accountListing.toList()
    val service = Executors.newFixedThreadPool(20)

    repeat(transactionCount) {
        val fromAccountNum = accNumber.random()
        val toAccountNum = accNumber.random()
        val k = (10..40).random()  // Диапазон перевода в %
        service.execute {
                bank.transfer(fromAccountNum, toAccountNum, bank.getBalance(fromAccountNum) / 100 * k)
        }
    }

    service.shutdown()
    service.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)

    // Ну да, такой вот ручник. Не хотелось всё на колаблы переделывать :(
    sleep(6000)

    // Итоговая проверка

    println("Печатаем состояние всех счетов")
    bank.printAccounts()
    println("Сумма всех вкладов в банке: ${bank.getSumAllBankDeposits()} \n")

    if (sumAllBankDeposits != bank.getSumAllBankDeposits()) {
        println("""Уважаемый, у вас дебит с кредитом не сходятся ¯\_(ツ)_/¯""")
    } else {
        println("По балансам всё сошлось\n")
    }

    println("""Транзакций запущено : $transactionCount""")
    // Т.к. одна транзакция логируется в обоих счетах то вдвое меньше беру
    println(
        """Транзакций выполнено: ${bank.getAllHistorySize() / 2}
        |
    """.trimMargin()
    )

    if (transactionCount != bank.getAllHistorySize() / 2) {
        println("""Уважаемый, у вас где-то проводки потерялись""")
    } else {
        println("По операциям всё сошлось")
    }
}
