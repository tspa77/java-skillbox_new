package com.tspa77

class Account(
    var money: Long,
    val accNumber: String
) : Comparable<Account> {
    var isBlocked = false
    val history = arrayListOf<String>()

    override fun toString(): String = "Number: $accNumber; \t is Blocked: $isBlocked;\t Money: $money"

    override fun compareTo(other: Account): Int {
        return this.accNumber.compareTo(other.accNumber)
    }
}
