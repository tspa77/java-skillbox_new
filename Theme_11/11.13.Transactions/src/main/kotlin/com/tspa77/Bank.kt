package com.tspa77

import java.util.*

class Bank {
    private val inspectionThreshold = 50_000L
    private val accounts = mutableSetOf<Account>()
    private val random = Random()

    // чтобы не отдавать наружу изменяемый список, даю его read-only копию
    val accountListing: Set<Account>
        get() = accounts.toSet()

    @Synchronized
    @Throws(InterruptedException::class)
    private fun isFraud(fromAccountNum: Account, toAccountNum: Account, amount: Long): Boolean {
        Thread.sleep(1000)
        return random.nextBoolean()
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами.
     * Если сумма транзакции > 50000, то после совершения транзакции,
     * она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка
     * счетов (как – на ваше усмотрение)
     */

    fun transfer(fromAccount: Account, toAccount: Account, amount: Long) {
        val (min, max) = listOf(fromAccount, toAccount).sorted()

        // Замораживаем счета на время проверки и перевода
        synchronized(min) {
            synchronized(max) {
                val requestText = ("Запрос на перевод $amount у.е. со счёта $fromAccount на счёт $toAccount\t")

                // Проверки
                if (isOutgoingAccountAvailable(fromAccount) &&
                    isBeneficiaryAccountAvailable(toAccount) &&
                    isEnoughMoney(fromAccount, amount) &&
                    isApprovedBySecurity(fromAccount, toAccount, amount)
                ) {
                    // Если всё Ок - переводим средства
                    fromAccount.money -= amount
                    toAccount.money += amount
                    fromAccount.history.add("$requestText - ВЫПОЛНЕН")
                    toAccount.history.add("$requestText - ВЫПОЛНЕН")
                } else {
                    // Если где-то что-то не срослось - логируем и выходим
                    fromAccount.history.add("$requestText - ОТКЛОНЁН")
                    toAccount.history.add("$requestText - ОТКЛОНЁН")
                }
            }
        }
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    fun getBalance(account: Account): Long {
        synchronized(account) {
            return account.money
        }
    }

    fun getSumAllBankDeposits() = accounts
        .map { it.money }
        .sum()

    fun getAllHistorySize() = accounts
        .map { it.history.size }
        .sum()

    fun addAccount(account: Account) {
        accounts.add(account)
    }

    fun printAccounts() {
        if (accounts.isEmpty()) {
            println("No accounts")
        } else {
//            accounts.forEach { (_, u) -> println(u) }
            accounts.forEach { println(it) }
            println()
        }
    }

    // Проверка что исходящий счёт не заблокирован
    private fun isOutgoingAccountAvailable(fromAccountNum: Account) =
        !fromAccountNum.isBlocked

    // Проверка что счёт получателя не заблокирован
    private fun isBeneficiaryAccountAvailable(toAccountNum: Account) =
        !toAccountNum.isBlocked

    // Проверка баланса на исходящем счету
    private fun isEnoughMoney(fromAccountNum: Account, amount: Long) =
        fromAccountNum.money >= amount

    // Проверка в СБ
    private fun isApprovedBySecurity(fromAccountNum: Account, toAccountNum: Account, amount: Long): Boolean {
        var approved = false
        if (amount >= inspectionThreshold) {
            try {
                if (isFraud(fromAccountNum, toAccountNum, amount)) {
                    fromAccountNum.isBlocked = true
                    toAccountNum.isBlocked = true
                } else {
                    approved = true
                }
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } else {
            approved = true
        }
        return approved
    }
}
