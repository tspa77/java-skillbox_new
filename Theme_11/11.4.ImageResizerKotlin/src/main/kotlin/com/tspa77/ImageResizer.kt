package com.tspa77

import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.roundToInt
import kotlin.system.measureTimeMillis


class ImageResizer(
    private val threadNumber: Int,
    private val listFiles: List<File>,
    private val dstFolder: String
) : Runnable {

    override fun run() {
        println("Поток $threadNumber получил на обработку ${listFiles.size} файлов")
        val time = measureTimeMillis {
            listFiles.forEach { resize(it, dstFolder) }
        }
        println("Поток $threadNumber выполнил работу за $time миилисекунд")
    }

    private fun resize(file: File, dstFolder: String) {
        try {
            var image = ImageIO.read(file)
            val targetWidth = 300 // Мэджик, конечно, но суть то не в нём, да ведь? А так в ТЗ было :)
            val targetHeight = (image.height / (image.width / targetWidth.toDouble())).roundToInt().toInt()
            var count = 0
            while (image.width > targetWidth) {
                var w = image.width / 2
                var h = image.height / 2
                if (w < targetWidth) {
                    w = targetWidth
                    h = targetHeight
                }
                val tmp = BufferedImage(w, h, image.type)
                val g2 = tmp.createGraphics()
                g2.setRenderingHint(
                    RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR
                )
                g2.drawImage(image, 0, 0, w, h, null)
                g2.dispose()
                image = tmp
                println("Поток $threadNumber, файл ${file.name} - итерация ${++count}")
            }
            val newFile = File(dstFolder + "/" + file.name)
            ImageIO.write(image, "jpg", newFile)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun resize2(file: File, dstFolder: String) {
        try {
            var image = ImageIO.read(file)

            // 600 NEAREST_NEIGHBOR
            var w = 600
            var h = (image.height / (image.width / w.toDouble())).roundToInt().toInt()

            var tmp = BufferedImage(w, h, image.type)
            var g2 = tmp.createGraphics()
            g2.setRenderingHint(
                RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR
            )
            g2.drawImage(image, 0, 0, w, h, null)
            g2.dispose()
            image = tmp

            // 300 BICUBIC
            w = 300
            h = (image.height / (image.width / w.toDouble())).roundToInt().toInt()

            tmp = BufferedImage(w, h, image.type)
            g2 = tmp.createGraphics()
            g2.setRenderingHint(
                RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BICUBIC
            )
            g2.drawImage(image, 0, 0, w, h, null)
            g2.dispose()
            image = tmp

            val newFile = File(dstFolder + "/" + file.name)
            ImageIO.write(image, "jpg", newFile)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
