package com.tspa77

import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import java.util.concurrent.Callable
import javax.imageio.ImageIO
import kotlin.math.roundToInt
import kotlin.system.measureTimeMillis

class ImageResizerCached(
    private val threadNumber: Int,
    private val file: File,
    private val dstFolder: String
) : Callable<Boolean> {

    override fun call(): Boolean {
        println("Поток $threadNumber получил на обработку файл")
        val time = measureTimeMillis {
            resize(file, dstFolder)
        }
        println("Поток $threadNumber выполнил работу за $time миилисекунд")
        return true
    }

    private fun resize(file: File, dstFolder: String) {
        try {
            var image = ImageIO.read(file)
            val targetWidth = 300 // Мэджик, конечно, но суть то не в нём, да ведь? А так в ТЗ было :)
            val targetHeight = (image.height / (image.width / targetWidth.toDouble())).roundToInt().toInt()
            var count = 0
            while (image.width > targetWidth) {
                var w = image.width / 2
                var h = image.height / 2
                if (w < targetWidth) {
                    w = targetWidth
                    h = targetHeight
                }
                val tmp = BufferedImage(w, h, image.type)
                val g2 = tmp.createGraphics()
                g2.setRenderingHint(
                    RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR
                )
                g2.drawImage(image, 0, 0, w, h, null)
                g2.dispose()
                image = tmp
                println("Поток $threadNumber, файл ${file.name} - итерация ${++count}")
            }
            val newFile = File(dstFolder + "/" + file.name)
            ImageIO.write(image, "jpg", newFile)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
