package com.tspa77

import java.util.stream.Collectors
import kotlin.math.ceil


fun separator(files: Int, cores: Int): List<IntRange> {
    if (files == 0) {
        return emptyList()
    }
    val result = arrayListOf<IntRange>()
    var currentLength = files
    var start = 0
    var end: Int
    for (i in 0 until cores) {
        // формируем блоки - если количество файлов не кратно кол-ву ядер, то округляем вверх
        val lengthBlock = ceil(currentLength.toDouble() / (cores - i)).toInt()
        end = start + lengthBlock
        result.add(start until end) // сохраняем результат
        // обновляем границы
        start = end
        currentLength -= lengthBlock
        // досрочный выход, когда количество файлов меньше количества ядер
        if (end >= files) {
            return result
        }
    }
    return result
}

fun separator2(files: Int, cores: Int): List<IntRange> {
    if (files == 0) {
        return emptyList()
    }
    // Считаем длину очереди для каждого ядра
    val blocks = arrayListOf<Int>()
    val blockSize = files / cores
    val surplus = files % cores
    // Распределяем основные блоки по "ядрам"
    for (i in 0 until cores) {
        blocks.add(blockSize)
    }
    // Раскидываем  остаток
    for (i in 0 until surplus) {
        blocks[i]++
    }
    // Убираем блоки с нулевой длиной (возможно когда количество файлов меньше количества ядер)
    val cropBlocks = blocks.stream().filter { it > 0 }.collect(Collectors.toList())
    // Переводим в промежутки / Range
    val bounds = arrayListOf<IntRange>()
    bounds.add(0 until cropBlocks[0])
    for (i in 1 until cropBlocks.size) {
        val prev = bounds[i - 1].last
        bounds.add(prev + 1..prev + cropBlocks[i])
    }
    return bounds
}
