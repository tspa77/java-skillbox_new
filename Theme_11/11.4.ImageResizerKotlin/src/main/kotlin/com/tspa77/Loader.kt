package com.tspa77

import java.io.File
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis


fun main() {
    val cores = Runtime.getRuntime().availableProcessors()

    val srcFolder = "C:\\Temp\\src"
    val dstFolder = "C:\\Temp\\dst"

    val srcDir = File(srcFolder)
    val files = srcDir.listFiles()

    if (files.isNullOrEmpty()) {
        return
    }

    // ImageResizer
//    val bounds = separator(files.size, cores)
//
//    for (i in bounds.indices) {
//        Thread(ImageResizer(i, files.slice(bounds[i]), dstFolder)).start()
//    }


    // ImageResizerCached
    val time = measureTimeMillis {
        val service = Executors.newCachedThreadPool()

        val listCall = arrayListOf<Callable<Boolean>>()

        for (i in files.indices) {
            listCall.add(ImageResizerCached(i, files[i], dstFolder))
        }

        listCall.forEach {
            service.submit(it)
        }

        service.apply {
            takeIf { true }
            shutdown()
            awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)
        }
    }
    println("Времени затрачено -  $time миллисекунд")
}
