package com.tspa77

import junit.framework.TestCase
import org.junit.Test

import org.junit.Assert.*

class SeparatorTest {

    @Test
    fun separator_0() {
        val expected = arrayListOf(0..0)
        val actual = separator(1, 8)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator_1() {
        val expected = arrayListOf(0..0, 1..1)
        val actual = separator(2, 4)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator_2() {
        val expected = arrayListOf(0..1, 2..3, 4..5, 6..7)
        val actual = separator(8, 4)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator_3() {
        val expected = arrayListOf(0..1, 2..3, 4..5, 6..6)
        val actual = separator(7, 4)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator_4() {
        val expected = arrayListOf(0..2, 3..4, 5..6, 7..8)
        val actual = separator(9, 4)
        TestCase.assertEquals(expected, actual)
    }
}


class Separator2Test {

    @Test
    fun separator2_0() {
        val expected = arrayListOf(0..0)
        val actual = separator2(1, 8)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator2_1() {
        val expected = arrayListOf(0..0, 1..1)
        val actual = separator2(2, 4)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator2_2() {
        val expected = arrayListOf(0..1, 2..3, 4..5, 6..7)
        val actual = separator2(8, 4)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator2_3() {
        val expected = arrayListOf(0..1, 2..3, 4..5, 6..6)
        val actual = separator2(7, 4)
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun separator2_4() {
        val expected = arrayListOf(0..2, 3..4, 5..6, 7..8)
        val actual = separator2(9, 4)
        TestCase.assertEquals(expected, actual)
    }
}